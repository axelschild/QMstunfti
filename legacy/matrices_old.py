import numpy as np
from scipy import sparse as sps
import time

'''
QMstunfti
by Axel Schild

This file is part of QMstunfti.

QMstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

QMstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with QMstunfti.  If not, see <http://www.gnu.org/licenses/>.


Desription:

Module containing functions to construct matrix representations of operators 
that can be used for eigenstate calculations and time-dependent propagations
in grid based representations of quantum mechanics.

Contains
 gradient_fd .... sparse matrix representation of the gradient operator using 
                  finite differences
 laplacian_fd ... sparse matrix representation of the laplacian operator using 
                  finite differences
                  NOTE: This version can use variable step widths, but this
                        feature is not well tested. The legacy version of this 
                        function is given at the end of the file.
 e_kin .......... convenience function to construct the matrix representation 
                  of a simple Cartesian kinetic energy operator
 pot_sparsefy ... convenience function to construct the matrix representation 
                  of an arbitrary n-dimensional potential
and some examples of potentials for certain selected problems.

'''

def gradient_fd(npts,spacings,speakF=False,reverseF=False,order=2):
  '''
  QMstunfti
  by Axel Schild
  
  Sparse matrix representation of the gradient operator using a finite 
  difference approximation of the derivatives.
  
  NOTE:
  This version can be used with variable step sizes. It can also be used if 
  there is a space-dependent parameter that multiplies the gradient. However,
  note that G = something / dx so that if e.g. f(x)*L is needed, the input 
  spacing should multiplied with 1/f(x) for the function to return f(x)*L.
  '''
  # make some checks
  npts = np.asarray(npts,dtype='int32')
  for ii in range(len(spacings)):
    spacings[ii] = np.asfarray(spacings[ii])
    if np.squeeze(spacings[ii].shape) and (np.squeeze(spacings[ii].shape) != npts[ii]):
      print('Spacing from dimension %d has the wrong number entries.' % ii)
      print('Should be just a number or have as smany entries as npts[%d].' % ii)
  try:
    if len(npts) != len(spacings):
      print('!input arrays do not have same length, result may be wrong!')
  except:
    print('!input arrays are probably of wrong type.')
  # if order is not reversed, reverse input order
  if not reverseF:
    npts = npts[::-1]
    spacings = spacings[::-1]
  # normalization of derivatives
  cfactor = []
  for ii in range(len(npts)):
    if   order == 2: 
      cfactor.append(np.power(spacings[ii],-1)/2.)
    elif order == 4: 
      cfactor.append(np.power(spacings[ii],-1)/12.)
    else: print ('Order has to be 2 or 4...this will (hopefully) crash...')
  # array containing the full matrices
  M_full = [[] for ii in range(len(npts))]
  # build the derivative matrices for each dimension
  for pdim in range(len(npts)):  
    if speakF == True: start = time.time()
    # size of current submatrix
    full_size = np.prod(npts[0:pdim+1])
    # Do the following if all grid spacings are equal for dimension pdim
    if not np.squeeze(spacings[pdim].shape):
      # produce current submatrix M with main diagonal (entries 0)
      M = sps.lil_matrix(0.*sps.eye(full_size))
      if order == 2:
        # distance between main diagonal and other diagonals
        shift1 = np.prod(npts[0:pdim])
        # produce other diagonals (with entries -1 (left) and +1 (right))
        for ii in range(shift1,full_size):
          M[ii,ii-shift1] = -1*cfactor[pdim]  
        for ii in range(full_size-shift1):
          M[ii,ii+shift1] =  1*cfactor[pdim]
      elif order == 4:
        # produce current submatrix M with main diagonal (entries 0)
        # distance between main diagonal and other diagonals
        shift1 = np.prod(npts[0:pdim])
        # produce other diagonals (with entries 1,-8,8,-1)
        for ii in range(2*shift1,full_size):
          M[ii,ii-2*shift1] =  1*cfactor[pdim]
        for ii in range(  shift1,full_size):
          M[ii,ii-  shift1] = -8*cfactor[pdim]
        for ii in range(full_size-shift1):
          M[ii,ii+  shift1] =  8*cfactor[pdim]
        for ii in range(full_size-2*shift1):
          M[ii,ii+2*shift1] = -1*cfactor[pdim]
    # Do the following for variable grid spacing
    else:
      # produce current submatrix M with main diagonal (entries 0)
      M = sps.lil_matrix(0.*sps.eye(full_size))
      if order == 2:
        
        # distance between main diagonal and other diagonals
        shift1 = np.prod(npts[0:pdim])
        # produce other diagonals (with entries 1 and -1)
        for ii in range(full_size-shift1):
          M[ii,ii+shift1] =  np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))[ii+shift1]
        for ii in range(shift1,full_size):
          M[ii,ii-shift1] = -np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))[ii-shift1]
      elif order == 4:
        # produce other diagonals (with entries 1,-8,8,-1)
        for ii in range(full_size-shift1):
          M[ii,ii+  shift1] =  8 * np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))[ii+  shift1]
        for ii in range(full_size-2*shift1):
          M[ii,ii+2*shift1] = -1 * np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))[ii+2*shift1]
        for ii in range(  shift1,full_size):
          M[ii,ii-  shift1] = -8 * np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))[ii-  shift1]
        for ii in range(2*shift1,full_size):
          M[ii,ii-2*shift1] =  1 * np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))[ii-2*shift1]
    # repeat the submatrix M as often as necessary
    # FIXME: there seems to be no nice way to use sps.block_diag() to repeat
    # the matrix M as often as needed
    stupid_string = (np.prod(npts[pdim+1:])-1) * 'M,'+'M'
    if pdim == len(npts)-1: M_full[pdim] = sps.coo_matrix(M)
    else: M_full[pdim] = sps.block_diag((eval(stupid_string)))
    if speakF == True: 
      end = time.time()
      print('time needed for dimension '\
            + str(pdim+1) + ': ' + str(end-start) + ' seconds')
  # return full matrices in sparse format
  if reverseF:
    return M_full
  else:
    return M_full[::-1]

def laplacian_fd(npts,spacings,speakF=False,reverseF=False,order=2,bc=[]):
  '''
  QMstunfti
  by Axel Schild
    
  Sparse matrix representation of the laplacian operator (more precise: the 
  second derivative operator in all spatial directions) using a finite 
  difference approximation of the derivatives.
  
  NOTE 1:
  This version can be used with variable step sizes. It can also be used if 
  there is a space-dependent parameter that multiplies the laplacian. However,
  note that L = something / dx**2 so that if e.g. f(x)*L is needed, the input 
  spacing should be multiplied by 1/sqrt(f(x)) for the function to return 
  f(x)*L. THIS IS ONLY IMPLEMENTED FOR DEFAULT BOUNDARY CONDITIONS!
  
  NOTE 2:
  This version has different boundary conditions implemented that can be used 
  with the variable bc. THIS IS NOT IMPLEMENTED FOR VARIABLE GRID SPACING!
  
  RETURNS: 
  M_full ..... len(npts) matrices, where M_full[ii] represents laplacian with 
               respect to coordinate ii, in a sparse martix format
  
  INPUT:
  npts ....... numpy array with number of points in each dimension, e.g. 
               np.array([10,15])
  spacings ... list with len(spacings) = len(npts); each entry is either a 
               number for the grid spacing or a one-dimensional numpy array
               with as many grid spacing as there are points for this directions
               (for variable grid spacing)
  reverseF ... Flag that indicates if reverse order shall be used (useful for
               comparison with SLEPC)
  order ...... Either 2 (2nd order) or 4 (4th order approximation).
  bc ......... List of strings that selects the boundary conditions for the 
               given spatial direction. If the list is empty, assume that in all
               directions condition 'd' is implied. For each dimension, give one 
               of the following strings:
                'd' .... default, ignore boundaries
                'c' .... stands for clever and uses forward or backward finite 
                         difference for left and right end [seems to be 
                         problematic for some reason]
                'r' .... reflecting boundary conditions [never really tested]
                'rd' ... left end reflecting, right end default
                'dr' ... left end default, right end reflecting
                'rc' ... left end reflecting, right end clever
                'cr' ... left end clever, right end reflecting
                'p' .... periodic
  
  NOTE 3:
  The code is written such that it calculates the Laplacian with the first 
  index varying fastest etc. This is called REVERSE order. If reverseF==False
  (default), it will swap the order of the inputs first to make the 
  calculation in  default order with the last index varying fastest.
  
  INDEPENDENT of the value of reverseF, the returned arrays are ordered such 
  that the first arrays corresponds to the first dimension etc., only the 
  layout of the arrays is different!
  
  EXPLANAITION:
  Example: 
  Calculate (d**2/dx**2 + d**2/dy**2) f(x,y). We assume that x and y
  are given on a regular grid with grid spacings dx, dy and grid values x(i)
  and y(j). Then f(x(i),y(j))=f(i,j).
  
  From a Taylor expansion of f(x(i)+dx,y(j)) and f(x(i)-dx,y(j)) we obtain up 
  to second order in dx:
  d**2f(i,j)/dx**2  = (f(i-1,j)-2*f(i,j)+f(i+1,j))/(dx**2) + O(dx**2)
  and analogously
  d**2f(i,j)/dy**2  = (f(i,j-1)-2*f(i,j)+f(i,j+1))/(dx**2) + O(dy**2).
  
  For fourth order in dx, we have e.g.:
  d**2f(i,j)/dx**2  = (-f(i-2,j)+16*f(i-1,j)-30*f(i,j)+16*f(i+1,j)-f(i+2,j))
                                                  /(12*dx**2) + O(dx**4)
  
  We now construct a vector g = (f(1,1),f(1,2),...,f(2,1),f(2,2),...) and 
  want to represent 
      d**2f(i,j)/dx**2 + d**2f(i,j)/dy**2
  as 
      g*M_full[0] + g*M_full[1].
  This function constructs the matrices M_full[ii].
  '''
  
  # make some checks
  npts = np.asarray(npts,dtype='int32')
  for ii in range(len(spacings)):
    spacings[ii] = np.asfarray(spacings[ii])
    if np.squeeze(spacings[ii].shape) and (np.squeeze(spacings[ii].shape) != npts[ii]):
      print('Spacing from dimension %d has the wrong number entries.' % ii)
      print('Should be just a number or have as smany entries as npts[%d].' % ii)
  try:
    if len(npts) != len(spacings):
      print('!input arrays do not have same length, result may be wrong!')
  except:
    print('!input arrays are probably of wrong type.')
  
  if not bc:
    bc = ['d' for ii in range(len(npts))]
  
  # if order is not reversed, reverse input order (see note above!)
  if not reverseF:
    npts = npts[::-1]
    spacings = spacings[::-1]
  # normalization of derivatives
  cfactor = []
  for ii in range(len(npts)):
    if   order == 2: 
      cfactor.append(np.power(spacings[ii],-2))
    elif order == 4: 
      cfactor.append(np.power(spacings[ii],-2)/12.)
    else: print ('Order has to be 2 or 4...this will (hopefully) crash...')
  # array containing the full matrices
  M_full = [[] for ii in range(len(npts))]
  # build the derivative matrices for each dimension
  for pdim in range(len(npts)):
    if speakF == True: start = time.time()
    # size of current submatrix
    full_size = np.prod(npts[0:pdim+1])
    # Do the following if all grid spacings are equal for dimension pdim
    if not np.squeeze(spacings[pdim].shape):
      # SECOND ORDER
      if order == 2:
        # produce current submatrix M with main diagonal (entries -2)
        M = sps.lil_matrix(-2 * cfactor[pdim] * sps.eye(full_size))
        # distance between main diagonal and other diagonals
        shift1 = np.prod(npts[0:pdim])
        # produce other diagonals (with entries 1)
        for ii in range(full_size-shift1):
          M[ii,ii+shift1] = cfactor[pdim]
        for ii in range(shift1,full_size):
          M[ii,ii-shift1] = cfactor[pdim]
        # apply boundary conditions
        if bc[pdim] == 'c' or bc[pdim] == 'cr':
          # clever boundary conditions for left end
          for ii in range(shift1):
            M[ii,ii] = cfactor[pdim]
            M[ii,ii+  shift1] = -2 * cfactor[pdim]
            M[ii,ii+2*shift1] =      cfactor[pdim]
        if bc[pdim] == 'c' or bc[pdim] == 'rc':
          # clever boundary conditions for right end
          for ii in range(shift1):
            M[-ii-1,-ii-1] = cfactor[pdim]
            M[-ii-1,-ii-1-  shift1] = -2 * cfactor[pdim]
            M[-ii-1,-ii-1-2*shift1] =      cfactor[pdim]
        if bc[pdim] == 'r' or bc[pdim] == 'rc' or bc[pdim] == 'rd':
          # reflective boundary conditions for left end
          for ii in range(shift1):
            M[ii,ii+  shift1] = 2 * cfactor[pdim]
        if bc[pdim] == 'r' or bc[pdim] == 'cr' or bc[pdim] == 'dr':
          # reflective boundary conditions for right end
          for ii in range(shift1):
            M[-ii-1,-ii-1-  shift1] = 2 * cfactor[pdim]
        if bc[pdim] == 'p':
          # periodic boundary conditions
          for ii in range(shift1):
            M[-shift1+ii,ii] = cfactor[pdim]
            M[ii,-shift1+ii] = cfactor[pdim]
      # FORTH ORDER
      elif order == 4:
        # produce current submatrix M with main diagonal (entries -30)
        M = sps.lil_matrix(-30 * cfactor[pdim] * sps.eye(full_size))
        # distance between main diagonal and other diagonals
        shift1 = np.prod(npts[0:pdim])
        # produce other diagonals (with entries 16 or -1)
        for ii in range(full_size-shift1):
          M[ii,ii+  shift1] = 16 * cfactor[pdim]
        for ii in range(full_size-2*shift1):
          M[ii,ii+2*shift1] = -1 * cfactor[pdim]
        for ii in range(  shift1,full_size):
          M[ii,ii-  shift1] = 16 * cfactor[pdim]
        for ii in range(2*shift1,full_size):
          M[ii,ii-2*shift1] = -1 * cfactor[pdim]
        # apply boundary conditions
        if bc[pdim] == 'c' or bc[pdim] == 'cr':
          # clever boundary conditions for left end
          for ii in range(shift1):
            M[ii,ii]          =   35 * cfactor[pdim]
            M[ii,ii+  shift1] = -104 * cfactor[pdim]
            M[ii,ii+2*shift1] =  114 * cfactor[pdim]
            M[ii,ii+3*shift1] =  -56 * cfactor[pdim]
            M[ii,ii+4*shift1] =   11 * cfactor[pdim]
            M[ii+shift1,ii]          =   11 * cfactor[pdim]
            M[ii+shift1,ii+  shift1] =  -20 * cfactor[pdim]
            M[ii+shift1,ii+2*shift1] =    6 * cfactor[pdim]
            M[ii+shift1,ii+3*shift1] =    4 * cfactor[pdim]
            M[ii+shift1,ii+4*shift1] =   -1 * cfactor[pdim]
        if bc[pdim] == 'c' or bc[pdim] == 'rc':
          # clever boundary conditions for right end
          for ii in range(shift1):
            M[-ii-1,-ii-1]          =   35 * cfactor[pdim]
            M[-ii-1,-ii-1-  shift1] = -104 * cfactor[pdim]
            M[-ii-1,-ii-1-2*shift1] =  114 * cfactor[pdim]
            M[-ii-1,-ii-1-3*shift1] =  -56 * cfactor[pdim]
            M[-ii-1,-ii-1-4*shift1] =   11 * cfactor[pdim]
            M[-ii-1-shift1,-ii-1]          =   11 * cfactor[pdim]
            M[-ii-1-shift1,-ii-1-  shift1] =  -20 * cfactor[pdim]
            M[-ii-1-shift1,-ii-1-2*shift1] =    6 * cfactor[pdim]
            M[-ii-1-shift1,-ii-1-3*shift1] =    4 * cfactor[pdim]
            M[-ii-1-shift1,-ii-1-4*shift1] =   -1 * cfactor[pdim]
        if bc[pdim] == 'r' or bc[pdim] == 'rc':
          # reflective boundary conditions for left end
          for ii in range(shift1):
            M[ii,ii+  shift1] = 32 * cfactor[pdim]
            M[ii,ii+2*shift1] = -2 * cfactor[pdim]
            M[ii+shift1,ii+  shift1] =  -31 * cfactor[pdim]
        if bc[pdim] == 'r' or bc[pdim] == 'cr':
          # reflective boundary conditions for right end
          for ii in range(shift1):
            M[-ii-1,-ii-1-  shift1] = 32 * cfactor[pdim]
            M[-ii-1,-ii-1-2*shift1] = -2 * cfactor[pdim]
            M[-ii-1-shift1,-ii-1-  shift1] =  -31 * cfactor[pdim]
        if bc[pdim] == 'p':
          # periodic boundary conditions
          for ii in range(shift1):
            M[-  shift1+ii,ii] = 16 * cfactor[pdim]
            M[ii,-  shift1+ii] = 16 * cfactor[pdim]
          for ii in range(2*shift1):
            M[-2*shift1+ii,ii] =     -cfactor[pdim]
            M[ii,-2*shift1+ii] =     -cfactor[pdim]
    
    # Do the following for variable grid spacing
    else:
      if order == 2:
        # produce current submatrix M with main diagonal (entries -2)
        M = sps.lil_matrix(-2 * sps.diags(np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))))
        # distance between main diagonal and other diagonals
        shift1 = np.prod(npts[0:pdim])
        # produce other diagonals (with entries 1)
        for ii in range(full_size-shift1):
          M[ii,ii+shift1] = np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))[ii+shift1]
        for ii in range(shift1,full_size):
          M[ii,ii-shift1] = np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))[ii-shift1]
      elif order == 4:
        # produce current submatrix M with main diagonal (entries -30)
        M = sps.lil_matrix(-30 * sps.diags(np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))))
        # distance between main diagonal and other diagonals
        shift1 = np.prod(npts[0:pdim])
        # produce other diagonals (with entries 16 or -1)
        for ii in range(full_size-shift1):
          M[ii,ii+  shift1] = 16 * np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))[ii+  shift1]
        for ii in range(full_size-2*shift1):
          M[ii,ii+2*shift1] = -1 * np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))[ii+2*shift1]
        for ii in range(  shift1,full_size):
          M[ii,ii-  shift1] = 16 * np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))[ii-  shift1]
        for ii in range(2*shift1,full_size):
          M[ii,ii-2*shift1] = -1 * np.repeat(cfactor[pdim],np.prod(npts[0:pdim]))[ii-2*shift1]
    
    # repeat the submatrix M as often as necessary
    # FIXME: there seems to be no nice way to use sps.block_diag() to repeat
    # the matrix M as often as needed
    stupid_string = (np.prod(npts[pdim+1:])-1) * 'M,'+'M'
    if pdim == len(npts)-1: M_full[pdim] = sps.coo_matrix(M)
    else: M_full[pdim] = sps.block_diag((eval(stupid_string)))
    
    if speakF == True: 
      end = time.time()
      print('time needed for dimension '\
            + str(pdim+1) + ': ' + str(end-start) + ' seconds')
  # return full matrices in sparse format
  if reverseF:
    return np.squeeze(M_full)
  else:
    return np.squeeze(M_full[::-1])

def e_kin(npts,delta,masses,reverseF=False,order=2,bc=[]):
  '''
  QMstunfti
  by Axel Schild
  
  Convenience function to compute the sparse matrix representation of a 
  simple Cartesian kinetic energy operator.
  
  Input:
    npts ..... numpy array with the number of points per dimension (even for one 
               dimension, this sould be something like npts = np.array([n])
               although the function tries to change this if you only give a 
               number
    delta .... numpy array with grid spacing per dimension
    masses ... numpy array with masses per dimension
  Output:
    sum_j -1/(2 masses[j]) * laplacian[j]
  '''
  try:
    len(npts)
    L = laplacian_fd(np.array(npts),np.array(delta),reverseF=reverseF,order=order,bc=bc)
    return sum(-1/(2*masses)*L)
  except:
    L = laplacian_fd(np.array([npts]),np.array([delta]),reverseF=reverseF,order=order,bc=bc)
    return -1/(2*masses)*L

def pot_sparsefy(V,reverseF=False):
  '''
  Convert an n-dimensional array V to sparse matrix operator representation
  (i.e., construct a sparse matrix that contains the entries of V on its 
  diagonal). By default, row-major order of the input array V is assumed
  (last index runs fastest), but by setting he optional parameter reverseF to
  True this may be changed to column-major order (first index varies fastest).
  '''
  if reverseF: 
    V = np.reshape(V,np.prod(np.shape(V)),order='F')
  else:
    V = np.reshape(V,np.prod(np.shape(V)),order='C')
  return sps.coo_matrix(sps.diags(V,0))
  

'''
Some example potentials follow:
  pot_1e1n_2d ... 1 electron and 1 nucleus (or havier particle) interacting 
                  with two fixed nuclei via a softened Coulomb potential, in 
                  a quartic external potential (to make the system bound)
  pot_2e_1d ..... 2 electrons in 1D interacting via a softened Coulomb potential
  pot_3e_1d ..... 3 electrons in 1D interacting via a softened Coulomb potential
  pot_efield .... potential of an electrical field in the dipole approximation, 
                  length gauge
'''

def pot_3e_1d(x1,x2,x3,R,Z,cee=0.,cen=0.,cnn=0.,vectorF=False,reverseF=False,sparseF=False,contribF=False):
  '''
  Molecular Soft-Coulomb potential
  '''
  # prepare potential
  V = np.zeros([len(x1),len(x2),len(x3)])
  # number of nuclei
  Nnuc = len(R)
  # internuclear interaction
  Vnn = 0.
  for i1 in range(Nnuc):
      for i2 in range(i1+1,Nnuc):
          Vnn += Z[i1]*Z[i2]*np.power(np.power(R[i1]-R[i2],2)+cnn,-0.5)
  # 2D grid to make further calculations at once
  try:
      X1,X2,X3 = np.meshgrid(x1,x2,x3,sparse=False,indexing='ij')
  except:
      X1,X2,X3 = np.meshgrid(x1,x2,x3)
      X1 = np.reshape(np.reshape(X1,np.prod(np.shape(X1)),'F'),np.shape(X1),'C')
      X2 = np.reshape(np.reshape(X2,np.prod(np.shape(X2)),'F'),np.shape(X2),'C')
      X3 = np.reshape(np.reshape(X3,np.prod(np.shape(X3)),'F'),np.shape(X3),'C')
  # electron-nucleus interaction
  Ve1n = np.zeros(len(x1))
  Ve2n = np.zeros(len(x2))
  Ve3n = np.zeros(len(x3))
  for i1 in range(Nnuc):
      Ve1n -= Z[i1]*np.power(np.power(R[i1]-x1,2)+cen,-0.5)
      Ve2n -= Z[i1]*np.power(np.power(R[i1]-x2,2)+cen,-0.5)
      Ve3n -= Z[i1]*np.power(np.power(R[i1]-x3,2)+cen,-0.5)
  Ven = np.reshape(Ve1n,(len(x1),1,1)) + np.reshape(Ve2n,(1,len(x2),1)) \
                                       + np.reshape(Ve3n,(1,1,len(x3)))
  # electron-electron interaction
  Vee = np.power(np.power(X1-X2,2)+cee,-0.5) + \
        np.power(np.power(X2-X3,2)+cee,-0.5) + \
        np.power(np.power(X3-X1,2)+cee,-0.5)
  # total potential
  V = np.nan_to_num(Vee) + np.nan_to_num(Ven) + np.nan_to_num(Vnn)
  # finalize output
  if vectorF or sparseF:
    if reverseF:
      V = np.reshape(V,len(x1)*len(x2)*len(x3),order='F')
    else:
      V = np.reshape(V,len(x1)*len(x2)*len(x3),order='C')
  else:
    if reverseF:
      V = np.reshape(V,len(x1)*len(x2)*len(x3),order='F')
      V = np.reshape(V,[len(x3),len(x2),len(x1)],order='C')
  if sparseF:
    if contribF:
      return sps.coo_matrix(sps.diags(V,0)), Ve1n, Ve2n, Ve3n, Vee, Vnn
    else:
      return sps.coo_matrix(sps.diags(V,0))
  else:
    if contribF:
      return V, Ve1n, Ve2n, Ve3n, Vee, Vnn
    else:
      return V

def potentialCation(x1,x2,R,Z,cee=0.,cen=0.,cnn=0.,vectorF=False,reverseF=False,sparseF=False,contribF=False):
  '''
  Molecular Soft-Coulomb potential
  '''
  # prepare potential
  V = np.zeros([len(x1),len(x2)])
  # number of nuclei
  Nnuc = len(R)
  # internuclear interaction
  Vnn = 0.
  for i1 in range(Nnuc):
      for i2 in range(i1+1,Nnuc):
          Vnn += Z[i1]*Z[i2]*np.power(np.power(R[i1]-R[i2],2)+cnn,-0.5)
  # 2D grid to make further calculations at once
  try:
      X1,X2 = np.meshgrid(x1,x2,sparse=False,indexing='ij')
  except:
      X1,X2 = np.meshgrid(x1,x2)
      X1 = np.reshape(np.reshape(X1,np.prod(np.shape(X1)),'F'),np.shape(X1),'C')
      X2 = np.reshape(np.reshape(X2,np.prod(np.shape(X2)),'F'),np.shape(X2),'C')
  # electron-nucleus interaction
  Ve1n = np.zeros(len(x1))
  Ve2n = np.zeros(len(x2))
  for i1 in range(Nnuc):
      Ve1n -= Z[i1]*np.power(np.power(R[i1]-x1,2)+cen,-0.5)
      Ve2n -= Z[i1]*np.power(np.power(R[i1]-x2,2)+cen,-0.5)
  Ven = np.reshape(Ve1n,(len(x1),1)) + np.reshape(Ve2n,(1,len(x2)))
  # electron-electron interaction
  Vee = np.power(np.power(X1-X2,2)+cee,-0.5)
  # total potential
  V = np.nan_to_num(Vee) + np.nan_to_num(Ven) + np.nan_to_num(Vnn)
  # finalize output
  if vectorF or sparseF:
    if reverseF:
      V = np.reshape(V,len(x1)*len(x2),order='F')
    else:
      V = np.reshape(V,len(x1)*len(x2),order='C')
  else:
    if reverseF:
      V = np.reshape(V,len(x1)*len(x2),order='F')
      V = np.reshape(V,[len(x2),len(x1)],order='C')
  if sparseF:
    if contribF:
      return sps.coo_matrix(sps.diags(V,0)), Ve1n, Ve2n, Vee, Vnn
    else:
      return sps.coo_matrix(sps.diags(V,0))
  else:
    if contribF:
      return V, Ve1n, Ve2n, Vee, Vnn
    else:
      return V

#def potentialCation(x1,R,Z,cee=0.,cen=0.,cnn=0.,reverseF=False,sparseF=False,contribF=False):
  #'''
  #Molecular Soft-Coulomb potential
  #'''
  ## prepare potential
  #V = np.zeros(len(x1))
  ## number of nuclei
  #Nnuc = len(R)
  ## internuclear interaction
  #Vnn = 0.
  #for i1 in range(Nnuc):
      #for i2 in range(i1+1,Nnuc):
          #Vnn += Z[i1]*Z[i2]*np.power(np.power(R[i1]-R[i2],2)+cnn,-0.5)
  ## electron-nucleus interaction
  #Ven = np.zeros(len(x1))
  #for i1 in range(Nnuc):
      #Ven -= Z[i1]*np.power(np.power(R[i1]-x1,2)+cen,-0.5)
  ## total potential
  #V = np.nan_to_num(Ven) + np.nan_to_num(Vnn)
  ## finalize output
  #if sparseF:
    #if contribF:
      #return sps.coo_matrix(sps.diags(V,0)), Ven, Vnn
    #else:
      #return sps.coo_matrix(sps.diags(V,0))
  #else:
    #if contribF:
      #return V, Ven, Vnn
    #else:
      #return V

def potentialField(F,x1,x2,vectorF=False,reverseF=False,sparseF=False):
  '''
  Electric field in length gauge, i.e. Efield = (x1+x2)*F(t)
  '''
  f1 = F*np.squeeze(x1)
  f2 = F*x2
  F1, F2 = np.meshgrid(f1,f2,sparse=False,indexing='ij')
  F = F1 + F2
  # finalize output
  if vectorF or sparseF:
    if reverseF:
      F = np.reshape(F,len(x1)*len(x2),order='F')
    else:
      F = np.reshape(F,len(x1)*len(x2),order='C')
  else:
    if reverseF:
      F = np.reshape(F,len(x1)*len(x2),order='F')
      F = np.reshape(F,[len(x2),len(x1)],order='C')
  if sparseF:
    return sps.coo_matrix(sps.diags(F,0))
  else:
    return F

def maskFunction(x1,x2,x1b,x2b,vectorF=False,reverseF=False):
  '''
  cos^(1/4) mask to reduce wavefunction at grid boundaries smoothly to zero
    x1b ... end of inner region in x1-direction
    x2b ... end of inner region in x2-direction
  NOTE: boundaries need to be symmetrical in positive and negative x-direction
  '''
  # x1-direction
  f1 = np.ones(len(x1))
  denominator = 2*(np.max(x1)-x1b)
  for i1, x1v in enumerate(x1):
    if x1v >= x1b:
      nominator = np.pi*(x1v-x1b)
      f1[i1] = (np.cos(nominator/denominator))**(0.25)
    elif x1v <= -x1b:
      nominator = np.pi*(-x1v-x1b)
      f1[i1] = (np.cos(nominator/denominator))**(0.25)
  # x2-direction
  f2 = np.ones(len(x2))
  denominator = 2*(np.max(x2)-x2b)
  for i2, x2v in enumerate(x2):
    if x2v >= x2b:
      nominator = np.pi*(x2v-x2b)
      f2[i2] = (np.cos(nominator/denominator))**(0.25)
    elif x2v <= -x2b:
      nominator = np.pi*(-x2v-x2b)
      f2[i2] = (np.cos(nominator/denominator))**(0.25)
  F1, F2 = np.meshgrid(f1,f2,sparse=False,indexing='ij')
  # full mask
  F = F1 * F2
  if vectorF or sparseF:
    if reverseF:
      F = np.reshape(F,len(x1)*len(x2),order='F')
    else:
      F = np.reshape(F,len(x1)*len(x2),order='C')
  else:
    if reverseF:
      F = np.reshape(F,len(x1)*len(x2),order='F')
      F = np.reshape(F,[len(x2),len(x1)],order='C')
  return F

# ******************************************************************************
# LEGACY FUNCTIONS (this can be trashed)
# ******************************************************************************
def laplacian_fd_legacy(npts,spacings,speakF=False,reverseF=False,order=2):
  '''
  Calculate matrix representation of the laplacian for len(npts) dimensions on 
  a grid using a second or fourth order approximation for the derivatives
  
  RETURNS: 
  M_full ..... len(npts) matrices, where M_full[ii] represents laplacian with 
                respect to coordinate ii, IN SPARSE MATRIX FORMAT
  
  INPUT:
  npts ....... numpy array with number of points in each dimension, e.g. 
                np.array([10,15])
  spacings ... numpy array with grid spacings in each dimension, e.g. 
                np.array([0.15,0.1])
  speakF ..... Flag that indicates if information shall be printed
  reverseF ... Flag that indicates if reverse order shall be used (useful for
                comparison with SLEPC)
  order ...... either 2 (2nd order) or 4 (4th order approximation)
                
  NOTE:
  The code is written such that it calculates the Laplacian with the first 
  index varying fastest etc. This is called REVERSE order. If reverseF==False
  (default), it will swap the order of the inputs first to make the 
  calculation in  default order with the last index varying fastest.
  
  INDEPENDENT of the value of reverseF, the returned arrays are ordered such 
  that the first arrays corresponds to the first dimension etc., only the 
  layout of the arrays is different!
  
  EXPLANAITION:
  Example: 
  Calculate (d**2/dx**2 + d**2/dy**2) f(x,y). We assume that x and y
  are given on a regular grid with grid spacings dx, dy and grid values x(i)
  and y(j). Then f(x(i),y(j))=f(i,j).
  
  From a Taylor expansion of f(x(i)+dx,y(j)) and f(x(i)-dx,y(j)) we obtain up 
  to second order in dx:
  d**2f(i,j)/dx**2  = (f(i-1,j)-2*f(i,j)+f(i+1,j))/(dx**2) + O(dx**2)
  and analogously
  d**2f(i,j)/dy**2  = (f(i,j-1)-2*f(i,j)+f(i,j+1))/(dx**2) + O(dy**2).
  
  For fourth order in dx, we have e.g.:
  d**2f(i,j)/dx**2  = (-f(i-2,j)+16*f(i-1,j)-30*f(i,j)+16*f(i+1,j)-f(i+2,j))
                                                  /(12*dx**2) + O(dx**4)
  
  We now construct a vector g = (f(1,1),f(1,2),...,f(2,1),f(2,2),...) and 
  want to represent 
      d**2f(i,j)/dx**2 + d**2f(i,j)/dy**2
  as 
      g*M_full[0] + g*M_full[1].
  This function constructs the matrices M_full[ii].
  '''
  
  # make some checks
  spacings = spacings.astype(float)
  npts = npts.astype(int)        
  try: 
    if len(npts) != len(spacings): 
      print('!input arrays do not have same length, result may be wrong!')
  except:
    print('!input arrays are probably of wrong type...need numpy arrays!')
  # if order is not reversed, reverse input order (see note above!)
  if not reverseF:
    npts = npts[::-1]
    spacings = spacings[::-1]
  # normalization of derivatives
  if   order == 2: cfactor = np.power(spacings,-2)
  elif order == 4: cfactor = np.power(spacings,-2)/12.
  else: print ('!order has to be 2 or 4...exiting...!')
  # array containing the full matrices
  M_full = [[] for ii in range(len(npts))]
  # build the derivative matrices for each dimension
  for pdim in range(len(npts)):  
    if speakF == True: start = time.time()
    # size of current submatrix
    full_size = np.prod(npts[0:pdim+1])
    if order == 2:
      # produce current submatrix M with main diagonal (entries -2)
      M = sps.lil_matrix(-2 * cfactor[pdim] * sps.eye(full_size))
      # distance between main diagonal and other diagonals
      shift1 = np.prod(npts[0:pdim])
      # produce other diagonals (with entries 1)
      for ii in range(full_size-shift1):
        M[ii,ii+shift1] = cfactor[pdim]
      for ii in range(shift1,full_size):
        M[ii,ii-shift1] = cfactor[pdim]  
    elif order == 4:
      # produce current submatrix M with main diagonal (entries -30)
      M = sps.lil_matrix(-30 * cfactor[pdim] * sps.eye(full_size))
      # distance between main diagonal and other diagonals
      shift1 = np.prod(npts[0:pdim])
      # produce other diagonals (with entries 16 or -1)
      for ii in range(full_size-shift1):
        M[ii,ii+  shift1] = 16 * cfactor[pdim]
      for ii in range(full_size-2*shift1):
        M[ii,ii+2*shift1] = -1 * cfactor[pdim]
      for ii in range(  shift1,full_size):
        M[ii,ii-  shift1] = 16 * cfactor[pdim]  
      for ii in range(2*shift1,full_size):
        M[ii,ii-2*shift1] = -1 * cfactor[pdim]  
    # repeat the submatrix M as often as necessary
    # FIXME: there seems to be no nice way to use sps.block_diag() to repeat
    # the matrix M as often as needed
    stupid_string = (np.prod(npts[pdim+1:])-1) * 'M,'+'M'
    if pdim == len(npts)-1: M_full[pdim] = sps.coo_matrix(M)
    else: M_full[pdim] = sps.block_diag((eval(stupid_string)))
    if speakF == True: 
      end = time.time()
      print('time needed for dimension '\
            + str(pdim+1) + ': ' + str(end-start) + ' seconds')
  # return full matrices in sparse format
  if reverseF:
    return np.squeeze(M_full)
  else:
    return np.squeeze(M_full[::-1])
