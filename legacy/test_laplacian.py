import numpy as np
from matrices import *
#np.set_printoptions(threshold=999)
np.core.arrayprint._line_width = 1200
np.set_printoptions(precision=2)
np.set_printoptions(threshold=np.nan)

## testing 1D, 2nd order
#l1do1 = laplacian_fd(np.array([6]),np.array([1]),order=2,bc=['c']).tolist()
#print('Laplacian 1D 2nd order')
#print(l1do1.todense())


## testing 1D, 2nd order
#l2do1 = laplacian_fd(np.array([5,5]),np.array([1,1]),order=2,bc=['p','p']).tolist()
#print('Laplacian 2D 2nd order, 1st dimension')
#print(l2do1[0].todense())
#print('Laplacian 2D 2nd order, 2nd dimension')
#print(l2do1[1].todense())


## testing 1D, 4th order
#l1do4 = laplacian_fd(np.array([6]),np.array([1]),order=4,bc=['c']).tolist()
#print('Laplacian 1D 2nd order')
#print(l1do4.todense())

# testing 1D, 4th order
l2do4 = laplacian_fd(np.array([5,5]),np.array([1,1]),order=4,bc=['p','p']).tolist()
print('Laplacian 2D 2nd order, 1st dimension')
print(l2do4[0].todense())
print('Laplacian 2D 2nd order, 2nd dimension')
print(l2do4[1].todense())
