import numpy as np
from   scipy import sparse as sps
import matplotlib.pyplot   as plt
from time               import time
from QMstunfti.matrices import gradient_fd, laplacian_fd, pot_sparsify
from QMstunfti.tools    import loadFromHDF5, saveToHDF5, integrate_over, drvtv_1, drvtv_2
plt.ion()

def make_grid(xmin,xmax,nx,xdim=0,ndim=0):
  x  = np.linspace(xmin,xmax,nx)
  dx = (x[-1]-x[0])/float(len(x)-1)
  if ndim:
    xsize = [1]*ndim
    xsize[xdim] = nx
    X = np.reshape(x,xsize)
    return x, dx, nx, X
  else:
    return x, dx, nx

np.set_printoptions(threshold=np.inf,linewidth=np.nan)
from scipy.linalg import toeplitz

x1pts, x2pts = 4, 4
x1del, x2del = 1, 1

npts       = np.array([x1pts,x2pts])
spacings   = np.array([x1del,x2del])
D1s, D2s   = gradient_fd( npts,spacings,order=2)
D11s, D22s   = laplacian_fd( npts,spacings,order=2)

d1 = toeplitz([0,1]+[0]*(x1pts-2)) 
for ii in range(1,x1pts): d1[ii,ii-1] *= -1
d1[0 , 0] = -2; d1[ 0, 1] = 2
d1[-1,-2] = -2; d1[-1,-1] = 2
d1 = d1/(2*x1del)

d2 = toeplitz([0,1]+[0]*(x2pts-2))
for ii in range(1,x2pts): d2[ii,ii-1] *= -1
d2[0 , 0] = -2; d2[ 0, 1] = 2
d2[-1,-2] = -2; d2[-1,-1] = 2
d2 = d2/(2*x2del)

D1  = np.kron(d1 ,np.eye(x2pts))
D2  = np.kron(np.eye(x1pts),d2 )

D1[np.abs(D1)<1e-6] = 0
D2[np.abs(D2)<1e-6] = 0




# grid definition
x1, x1del, x1pts, X1 = make_grid(-6,6,100,xdim=0,ndim=2)
x2, x2del, x2pts, X2 = make_grid(-6,6,100,xdim=1,ndim=2)

# function to look at and its derivatives
fexp   =  np.exp(-(X2+1)**2/5)
V      =  np.sin(X1) * np.cos(X2) * fexp
dV_d1  =  np.cos(X1) * np.cos(X2) * fexp
dV_d2  = -np.sin(X1) * (5*np.sin(X2)+2*(X2+1)*np.cos(X2)) * fexp / 5
dV_d11 = -np.sin(X1) * np.cos(X2) * fexp
dV_d22 =  np.sin(X1) * (4/5*fexp*(X2+1)*np.sin(X2) - fexp*np.cos(X2) + \
            np.cos(X2) * (4/25*fexp*(X2+1)**2 - 2/5*fexp) )

# sparse matrix operator setup
npts       = np.array([x1pts,x2pts])
spacings   = np.array([x1del,x2del])
D1s, D2s   = gradient_fd( npts,spacings,order=2)
D11s, D22s = laplacian_fd(npts,spacings,order=2)

# application to the vector
V_vec       = np.reshape(V,x1pts*x2pts)
dV_d1_mats  = (D1s *V_vec).reshape(x1pts,x2pts)
dV_d2_mats  = (D2s *V_vec).reshape(x1pts,x2pts)
dV_d11_mats = (D11s*V_vec).reshape(x1pts,x2pts)
dV_d22_mats = (D22s*V_vec).reshape(x1pts,x2pts)

#cmax = dV_d1.max()
#plt.figure(figsize=(12,3))
#plt.subplot(1,3,1)
#plt.title(r'$f(x,y)$')
#plt.contourf(x1,x2,V.transpose(),np.linspace(-V.max(),V.max(),100),cmap='RdBu')
#plt.subplot(1,3,2)
#plt.title(r'$\partial_x f$, sparse matrix')
#plt.contourf(x1,x2,dV_d1_mats.transpose(),np.linspace(-cmax,cmax,100),cmap='RdBu')
#plt.subplot(1,3,3)
#plt.title(r'$\partial_x f$')
#plt.contourf(x1,x2,dV_d1.transpose(),np.linspace(-cmax,cmax,100),cmap='RdBu')

tval = np.max( np.abs( dV_d1_mats - dV_d1) )
tval_loc = np.unravel_index(np.argmax( np.abs( dV_d1_mats - dV_d1) ),(x1pts,x2pts))
print(' maximum difference, 1st derivative, x1: %10.5f @ x=%10.5f, y=%10.5f' % (tval,x1[tval_loc[0]],x2[tval_loc[1]]))


#cmax = dV_d2.max()
#plt.figure(figsize=(12,3))
#plt.subplot(1,3,1)
#plt.title(r'$f(x,y)$')
#plt.contourf(x1,x2,V.transpose(),np.linspace(-V.max(),V.max(),100),cmap='RdBu')
#plt.subplot(1,3,2)
#plt.title(r'$\partial_y f$, sparse matrix')
#plt.contourf(x1,x2,dV_d2_mats.transpose(),np.linspace(-cmax,cmax,100),cmap='RdBu')
#plt.subplot(1,3,3)
#plt.title(r'$\partial_y f$')
#plt.contourf(x1,x2,dV_d2.transpose(),np.linspace(-cmax,cmax,100),cmap='RdBu')

tval = np.max( np.abs( dV_d2_mats - dV_d2) )
tval_loc = np.unravel_index(np.argmax( np.abs( dV_d2_mats - dV_d2) ),(x1pts,x2pts))
print(' maximum difference, 1st derivative, x2: %10.5f @ x=%10.5f, y=%10.5f' % (tval,x1[tval_loc[0]],x2[tval_loc[1]]))


#cmax = dV_d11.max()
#plt.figure(figsize=(12,3))
#plt.subplot(1,3,1)
#plt.title(r'$f(x,y)$')
#plt.contourf(x1,x2,V.transpose(),np.linspace(-V.max(),V.max(),100),cmap='RdBu')
#plt.subplot(1,3,2)
#plt.title(r'$\partial_x^2 f$, sparse matrix')
#plt.contourf(x1,x2,dV_d11_mats.transpose(),np.linspace(-cmax,cmax,100),cmap='RdBu')
#plt.subplot(1,3,3)
#plt.title(r'$\partial_x^2 f$')
#plt.contourf(x1,x2,dV_d11.transpose(),np.linspace(-cmax,cmax,100),cmap='RdBu')

tval = np.max( np.abs( dV_d11_mats - dV_d11) )
tval_loc = np.unravel_index(np.argmax( np.abs( dV_d11_mats - dV_d11) ),(x1pts,x2pts))
print(' maximum difference, 2st derivative, x1: %10.5f @ x=%10.5f, y=%10.5f' % (tval,x1[tval_loc[0]],x2[tval_loc[1]]))


#cmax = dV_d22.max()
#plt.figure(figsize=(12,3))
#plt.subplot(1,3,1)
#plt.title(r'$f(x,y)$')
#plt.contourf(x1,x2,V.transpose(),np.linspace(-V.max(),V.max(),100),cmap='RdBu')
#plt.subplot(1,3,2)
#plt.title(r'$\partial_y^2 f$, sparse matrix')
#plt.contourf(x1,x2,dV_d22_mats.transpose(),np.linspace(-cmax,cmax,100),cmap='RdBu')
#plt.subplot(1,3,3)
#plt.title(r'$\partial_y^2 f$')
#plt.contourf(x1,x2,dV_d22.transpose(),np.linspace(-cmax,cmax,100),cmap='RdBu')

tval = np.max( np.abs( dV_d22_mats - dV_d22) )
tval_loc = np.unravel_index(np.argmax( np.abs( dV_d22_mats - dV_d22) ),(x1pts,x2pts))
print(' maximum difference, 2st derivative, x2: %10.5f @ x=%10.5f, y=%10.5f' % (tval,x1[tval_loc[0]],x2[tval_loc[1]]))
