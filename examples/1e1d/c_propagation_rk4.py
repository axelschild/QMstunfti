import time
import numpy               as np
import scipy.sparse.linalg as linalg
import matplotlib.pyplot   as plt
from scipy              import sparse as sps
from scipy.special      import erf
from QMstunfti.matrices import e_kin, pot_sparsify
from QMstunfti.tools    import loadFromHDF5, saveToHDF5, integrate_over
from QMstunfti.psi      import make_psi, mask_1d
from QMstunfti          import ptools
plt.ion()

'''
  Propagate the 2-electron-1D soft-Coulomb model
  
  For the real propagation, change the grid in c_eigenstates.py to
    x1min = -100.0; x1max = 100.0; x1pts = 1000
    x2min = -100.0; x2max = 100.0; x2pts = 1000
  and add a NIP/CAP/mask
'''

def laserfield(dt=0.01,w0=0.0857,f0=0.2,n_cyc=12,n_max=14,n_on=2,n_off=2):
  '''
    Get time grid and laser field
      dt ...... time step
      w0 ...... laser field freqency
      f0 ...... laser field amplitude
      n_cyc ... number of laser cycles
      n_max ... maximum of time grid in numbers of laser cycle
      n_on .... time to turn on  laser field in numbers of laser cycle
      n_off ... time to turn off laser field in numbers of laser cycle
  '''
  
  # define the time grid
  T    = 2 * np.pi / w0
  tmax = n_max * T
  t    = np.arange(0,tmax+dt,dt)
  # oscillation
  oscillation = f0 * np.cos(w0 * t)
  # envelope, quadratic turn on and off
  envelope    = np.ones(len(t))
  i_on  = t<n_on*T
  t_on  = t[i_on]
  i_off = (t>(n_cyc-n_off)*T) & (t<n_cyc*T)
  t_off = t[i_off]
  envelope[i_on]  = (t_on / t_on.max())**2
  envelope[i_off] = (((t_off - t_off[0]) / (t_off - t_off[0]).max())[::-1])**2
  envelope[t>n_cyc*T] = 0.
  return t, envelope * oscillation

# load data from eigenvalue calculation
fname = 'data_1e1d.h5'
data = loadFromHDF5(fname)
for key,val in data.items(): exec(key + '=val')

# time grid and laser field
t, field = laserfield(dt=0.005)
t_pts = len(t)
t_del = (t[-1]-t[0])/float(t_pts-1)

plt.figure('laserfield')
plt.plot(t,field)
plt.pause(.1)

# number of sub steps where no wavefunction is saved 
nt_sub  = 1000
nt_main = t_pts // nt_sub

# grid parameters
x1pts = len(x1)
x1del = (x1[-1]-x1[0]) / float(x1pts-1)

# initial state
psi0 = psi[0]
psi  = psi0.astype(np.complex128)

# mask function
mask = mask_1d(x1,b=0.9,power=0.125)

plt.figure('density')
plt.plot(x1,np.log(np.abs(psi0)**2))
plt.axis([x1[0],x1[-1],-20,1])
plt.pause(.1)

# propagation
i_run = 0
norm = np.sum(np.abs(psi)**2)*x1del
for i_main in range(nt_main):
  psi    = ptools.rk4real_1d_laser(psi,V,mask,x1,field[i_main*nt_sub:(i_main+1)*nt_sub+1],x1del,t_del,m1,nt_sub)
  i_run += nt_sub
  
  #saveToHDF5('psi_%03d.h5' % i_main, {'psi':np.reshape(vec,(x1pts,x2pts))})
  plt.figure('laserfield')
  plt.clf()
  plt.plot(t,field)
  plt.plot(t[i_run],field[i_run],'o')
  plt.pause(.1)
  
  plt.figure('density')
  plt.clf()
  plt.plot(x1,np.log(np.abs(psi)**2))
  plt.axis([x1[0],x1[-1],-20,1])
  plt.pause(.1)
  #print('%d out of %d' % (i_main+1,nt_main))
  
  #sys.exit()
