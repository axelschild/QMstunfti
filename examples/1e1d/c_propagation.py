import time
import numpy               as np
import scipy.sparse.linalg as linalg
import matplotlib.pyplot   as plt
from scipy                 import sparse as sps
from scipy.special         import erf
from QMstunfti.matrices    import e_kin, pot_sparsify
from QMstunfti.tools       import loadFromHDF5, saveToHDF5, integrate_over
from QMstunfti.psi         import make_psi, mask_1d
from QMstunfti.propagators import prop_expm_multiply, crank_nicholson
plt.ion()

'''
  Propagate the 2-electron-1D soft-Coulomb model
  
  For the real propagation, change the grid in c_eigenstates.py to
    x1min = -100.0; x1max = 100.0; x1pts = 1000
    x2min = -100.0; x2max = 100.0; x2pts = 1000
  and add a NIP/CAP/mask
'''

def plot_fun(t,field,x1,vec,label,i_run):
  
  plt.figure('propagation',figsize=(14,6))
  plt.clf()
  plt.subplot(1,2,1)
  plt.plot(t,field)
  plt.plot(t[i_run],field[i_run],'o')
  plt.xlabel('t in a.u.')
  plt.ylabel('F in a.u.')
  plt.axis([t[0],t[-1],1.1*np.min(field),1.1*np.max(field)])
  
  plt.subplot(1,2,2)
  plt.plot(x1,np.log(np.abs(vec[0])**2),label=label[0])
  plt.plot(x1,np.log(np.abs(vec[1])**2),label=label[1])
  plt.axis([x1[0],x1[-1],-20,1])
  plt.xlabel('x in a.u.')
  plt.ylabel('$\log |\psi|^2$')
  plt.legend()
  plt.pause(.1)

def laserfield(dt=0.01,w0=0.0857,f0=0.14,n_cyc=6,n_max=7,n_on=2,n_off=2):
  '''
    Get time grid and laser field
      dt ...... time step
      w0 ...... laser field freqency
      f0 ...... laser field amplitude
      n_cyc ... number of laser cycles
      n_max ... maximum of time grid in numbers of laser cycle
      n_on .... time to turn on  laser field in numbers of laser cycle
      n_off ... time to turn off laser field in numbers of laser cycle
  '''
  
  # define the time grid
  T    = 2 * np.pi / w0
  tmax = n_max * T
  t    = np.arange(0,tmax+dt,dt)
  # oscillation
  oscillation = f0 * np.cos(w0 * t)
  # envelope, quadratic turn on and off
  envelope    = np.ones(len(t))
  i_on  = t<n_on*T
  t_on  = t[i_on]
  i_off = (t>(n_cyc-n_off)*T) & (t<n_cyc*T)
  t_off = t[i_off]
  envelope[i_on]  = (t_on / t_on.max())**2
  envelope[i_off] = (((t_off - t_off[0]) / (t_off - t_off[0]).max())[::-1])**2
  envelope[t>n_cyc*T]         = 0.
  return t, envelope * oscillation

# load data from eigenvalue calculation
fname = 'data_1e1d.h5'
data = loadFromHDF5(fname)
for key,val in data.items(): exec(key + '=val')

# time grid and laser field
t, field = laserfield(dt=0.025)
t_pts = len(t)
t_del = (t[-1]-t[0])/float(t_pts-1)

# number of sub steps where no wavefunction is saved 
nt_sub  = 400
nt_main = t_pts // nt_sub

# grid parameters
x1pts = len(x1)
x1del = (x1[-1]-x1[0]) / float(x1pts-1)

# initial state
psi0 = psi[0]
vec  = np.reshape(psi0,x1pts).astype(np.complex128)

# mask function
mask = mask_1d(x1,b=0.9,power=0.125)

# set up the hamiltonian
masses    = np.array([m1])
npts      = np.array([x1pts])
spacings  = np.array([x1del])
E_kin     = e_kin(npts,spacings,masses,order=4)
E_pot     = pot_sparsify(V)

vec_t1 = vec
vec_t2 = vec

# propagation
for i_main in range(nt_main):
  field_now = field[i_main*nt_sub:(i_main+1)*nt_sub]
  vec_t1 = prop_expm_multiply(vec_t1,t_del=t_del,H_ti=E_kin+E_pot,field=field_now,dipole=x1,mask=mask)
  vec_t2 = crank_nicholson(vec_t2,nt_sub,t_del,E_kin+E_pot,field_now,pot_sparsify(x1),mask=mask)
  plot_fun(t,field,x1,[vec_t1,vec_t2],['expm_multiply','Crank-Nicholson'],(i_main+1)*nt_sub)
  

# final steps
field_now = field[i_main*nt_sub:]
vec_t1 = prop_expm_multiply(vec_t1,t_del=t_del,H_ti=E_kin+E_pot,field=field_now,dipole=x1,mask=mask)
vec_t2 = crank_nicholson(vec_t2,nt_sub,t_del,E_kin+E_pot,field_now,pot_sparsify(x1),mask=mask)  
plot_fun(t,field,x1,[vec_t1,vec_t2],['expm_multiply','Crank-Nicholson'],(i_main+1)*nt_sub)
