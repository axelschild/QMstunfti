import time
import numpy               as np
import scipy.sparse.linalg as linalg
import matplotlib.pyplot   as plt
from scipy              import sparse as sps
from QMstunfti.matrices import e_kin, pot_sparsify
from QMstunfti.tools    import saveToHDF5, integrate_over
from QMstunfti.psi      import make_psi
plt.ion()  # turn interactive plotting on 

'''
  Compute eigenstates of a 1-electron-1D soft-Coulomb model
'''

def pot_soft_coulomb(x1,R,Z,cen,cnn):
  # number of nuclei
  Nnuc = len(R)
  # internuclear interaction
  Vnn = 0.
  for i1 in range(Nnuc):
    for i2 in range(i1+1,Nnuc):
      Vnn += Z[i1]*Z[i2]*np.power(np.power(R[i1]-R[i2],2)+cnn,-0.5)
  # electron-nucleus interaction
  Ven = np.zeros(len(x1))
  for i1 in range(Nnuc):
    Ven -= Z[i1]*np.power(np.power(R[i1]-x1,2)+cen,-0.5)
  # total potential
  return np.nan_to_num(Ven) + np.nan_to_num(Vnn)

# *** parameters ***
x1min = -40.0; x1max = 40.0; x1pts = 400
masses = np.array([1.])
cen = 0.55; cnn = 1.;

# *** grids ***
x1 = np.linspace(x1min,x1max,x1pts); x1del = (x1max-x1min)/float(x1pts-1);

# *** nuclear positions and charges***
R = [0.]
Z = [2.]

# ------------------------------------------------------------------------------
t_start = time.time(); 
print('Starting eigenstate calculation')

# *** eigenstate computation ***
nstates = 2

# *** kinetic energy matrix ***
npts      = np.array([x1pts])
spacings  = np.array([x1del])
E_kin     = e_kin(npts,spacings,masses,order=4)

# *** potential energy matrix ***
V         = pot_soft_coulomb(x1,R,Z,cen,cnn)
E_pot     = pot_sparsify(V)

# *** eigenvalues and eigenfunctions ***
vals, vecs = linalg.eigsh(E_kin + E_pot,k=nstates,which='SA')

# *** normalize eigenfunctions ***
psi = make_psi(vecs,npts,x1del,nstates=nstates)

t_end = time.time(); 
print('Calculation took '+str(t_end-t_start)+' seconds.')

stuff = {'x1':x1, 'V':V, 'psi':psi, 'E':vals, 'm1': masses[0]}
fname = 'data_1e1d.h5'
saveToHDF5(fname,stuff)
