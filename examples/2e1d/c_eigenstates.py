import time
import numpy               as np
import scipy.sparse.linalg as linalg
import matplotlib.pyplot   as plt
from scipy              import sparse as sps
from QMstunfti.matrices import e_kin, pot_sparsify
from QMstunfti.tools    import saveToHDF5, integrate_over
from QMstunfti.psi      import make_psi
plt.ion()  # turn interactive plotting on 

'''
  Compute eigenstates of a 2-electron-1D soft-Coulomb model
'''

def pot_soft_coulomb(x1,x2,R,Z,cee,cen,cnn):
  # number of nuclei
  Nnuc = len(R)
  # internuclear interaction
  Vnn = 0.
  for i1 in range(Nnuc):
    for i2 in range(i1+1,Nnuc):
      Vnn += Z[i1]*Z[i2]*np.power(np.power(R[i1]-R[i2],2)+cnn,-0.5)
  # reshape grid to make further calculations at once
  X1 = np.reshape(x1,(len(x1),1))
  X2 = np.reshape(x2,(1,len(x1)))
  # electron-nucleus interaction
  Ve1n = np.zeros(len(x1))
  Ve2n = np.zeros(len(x2))
  for i1 in range(Nnuc):
      Ve1n -= Z[i1]*np.power(np.power(R[i1]-x1,2)+cen,-0.5)
      Ve2n -= Z[i1]*np.power(np.power(R[i1]-x2,2)+cen,-0.5)
  Ven = np.reshape(Ve1n,(len(x1),1)) + Ve2n
  # electron-electron interaction
  Vee = np.power(np.power(X1-X2,2)+cee,-0.5)
  # total potential
  Vtot = np.nan_to_num(Vee) + np.nan_to_num(Ven) + np.nan_to_num(Vnn)
  return Vtot, Vee, Ve1n, Vnn

# *** parameters ***
x1min = -20.0; x1max = 20.0; x1pts = 150
x2min = -20.0; x2max = 20.0; x2pts = 150
masses = np.array([1.,1.])
cee = 0.55; cen = 0.55; cnn = 1.;

# *** grids ***
x1 = np.linspace(x1min,x1max,x1pts); x1del = (x1max-x1min)/float(x1pts-1);
x2 = np.linspace(x2min,x2max,x2pts); x2del = (x2max-x2min)/float(x2pts-1);

# *** nuclear positions and charges***
R = [0.]
Z = [2.]

# ------------------------------------------------------------------------------
t_start = time.time(); 
print('Starting eigenstate calculation')

# *** eigenstate computation ***
nstates = 2

# *** kinetic energy matrix ***
npts      = np.array([x1pts,x2pts])
spacings  = np.array([x1del,x2del])
E_kin     = e_kin(npts,spacings,masses,order=4)

# *** potential energy matrix ***
V, V_ee, V_en, V_nn  = pot_soft_coulomb(x1,x2,R,Z,cee,cen,cnn)
E_pot                = pot_sparsify(V)

# *** eigenvalues and eigenfunctions ***
vals, vecs = linalg.eigsh(E_kin + E_pot,k=nstates,which='SA')

# *** normalize eigenfunctions ***
psi = make_psi(vecs,npts,x1del*x2del,nstates=nstates)

t_end = time.time(); 
print('Calculation took '+str(t_end-t_start)+' seconds.')

stuff = {'x1':x1, 'x2':x2, 'V':V, 'V_ee':V_ee, 'V_en':V_en, 'V_nn':V_nn, 
         'psi':psi, 'E':vals, 'm1': masses[0], 'm2': masses[1]}
fname = 'data_2e1d.h5'
saveToHDF5(fname,stuff)

plt.figure(figsize=(10,4.5))

plt.subplot(1,2,1)
plt.title('$\psi_0$')
cmax = np.abs(psi[0]).max()
plt.contourf(x1,x2,psi[0],np.linspace(-cmax,cmax,50),cmap='RdBu')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')

plt.subplot(1,2,2)
plt.title('$\psi_1$')
cmax = np.abs(psi[1]).max()
plt.contourf(x1,x2,psi[1],np.linspace(-cmax,cmax,50),cmap='RdBu')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
