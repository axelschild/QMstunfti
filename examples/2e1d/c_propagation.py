import time
import numpy               as np
import scipy.sparse.linalg as linalg
import matplotlib.pyplot   as plt
from scipy              import sparse as sps
from scipy.special      import erf
from QMstunfti.matrices import e_kin, pot_sparsify
from QMstunfti.tools    import loadFromHDF5, saveToHDF5, integrate_over
from QMstunfti.psi      import make_psi, mask_2d
plt.ion()

'''
  Propagate the 2-electron-1D soft-Coulomb model
  
  For the real propagation, change the grid in c_eigenstates.py to
    x1min = -100.0; x1max = 100.0; x1pts = 1000
    x2min = -100.0; x2max = 100.0; x2pts = 1000
  and add a NIP/CAP/mask
'''

def laserfield(dt=0.01,w0=0.0857,f0=0.14,n_cyc=12,n_max=14,n_on=2,n_off=2):
  '''
    Get time grid and laser field
      dt ...... time step
      w0 ...... laser field freqency
      f0 ...... laser field amplitude
      n_cyc ... number of laser cycles
      n_max ... maximum of time grid in numbers of laser cycle
      n_on .... time to turn on  laser field in numbers of laser cycle
      n_off ... time to turn off laser field in numbers of laser cycle
  '''
  
  # define the time grid
  T    = 2 * np.pi / w0
  tmax = n_max * T
  t    = np.arange(0,tmax+dt,dt)
  # oscillation
  oscillation = f0 * np.cos(w0 * t)
  # envelope, quadratic turn on and off
  envelope    = np.ones(len(t))
  i_on  = t<n_on*T
  t_on  = t[i_on]
  i_off = (t>(n_cyc-n_off)*T) & (t<n_cyc*T)
  t_off = t[i_off]
  envelope[i_on]  = (t_on / t_on.max())**2
  envelope[i_off] = (((t_off - t_off[0]) / (t_off - t_off[0]).max())[::-1])**2
  envelope[t>n_cyc*T]         = 0.
  return t, envelope * oscillation

# load data from eigenvalue calculation
fname = 'data_2e1d.h5'
data = loadFromHDF5(fname)
for key,val in data.items(): exec(key + '=val')

# time grid and laser field
t, field = laserfield(dt=0.1)
t_pts = len(t)
t_del = (t[-1]-t[0])/float(t_pts-1)

plt.figure('laserfield')
plt.plot(t,field)
plt.pause(.1)

# number of sub steps where no wavefunction is saved 
nt_sub  = 100
nt_main = t_pts // nt_sub

# grid parameters
x1pts = len(x1)
x2pts = len(x2)
x1del = (x1[-1]-x1[0]) / float(x1pts-1)
x2del = (x2[-1]-x2[0]) / float(x2pts-1)

# initial state
psi0 = psi[0]
vec  = np.reshape(psi0,x1pts*x2pts).astype(np.complex128)

# mask function
mask = mask_2d(x1,x2,bx=0.9,by=0.9,power=0.125)

# set up the hamiltonian
masses    = np.array([m1,m2])
npts      = np.array([x1pts,x2pts])
spacings  = np.array([x1del,x2del])
E_kin     = e_kin(npts,spacings,masses,order=4)
E_pot     = pot_sparsify(V)

plt.figure('density')
plt.contourf(x1,x2,np.log(np.abs(psi0.transpose())**2),np.linspace(-20,1,30),cmap='afmhot_r')
plt.pause(.1)

# dipole grid
dip = np.reshape(x1,(x1pts,1)) + np.reshape(x2,(1,x2pts))

# propagation
i_run = 0
for i_main in range(nt_main):
  for i_sub in range(nt_sub):
    # add laser field to potential and propagate
    U    = -1j * (E_kin + E_pot + pot_sparsify(field[i_run] * dip)) * t_del
    vec  = linalg.expm_multiply(U,vec)
    i_run += 1
    print(i_sub)
  
  #saveToHDF5('psi_%03d.h5' % i_main, {'psi':np.reshape(vec,(x1pts,x2pts))})
  plt.figure('laserfield')
  plt.clf()
  plt.plot(t,field)
  plt.plot(t[i_run],field[i_run],'o')
  plt.pause(.1)
  
  plt.figure('density')
  plt.clf()
  plt.contourf(x1,x2,np.log(np.abs(np.reshape(vec,(x1pts,x2pts)).transpose())**2),np.linspace(-20,1,30),cmap='afmhot_r')
  plt.pause(.1)
  print('%d out of %d' % (i_main+1,nt_main))
