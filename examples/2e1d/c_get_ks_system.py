import numpy               as np
import matplotlib.pyplot   as plt
import scipy.sparse.linalg as linalg
import sys
from QMstunfti.tools      import loadFromHDF5, saveToHDF5, drvtv_1, drvtv_2
from QMstunfti.psi        import make_psi
from QMstunfti.matrices   import e_kin, pot_sparsify

# load data from eigenvalue calculation
fname    = 'data_2e1d.h5'
data     = loadFromHDF5(fname)
for key,val in data.items(): exec(key + '=val')

n_state = 1
n_el    = 2
c_mu    = 1.0
c_p     = 0.1

n1 = len(x1); d1 = (x1[-1]-x1[0])/float(n1-1)
n2 = len(x2); d2 = (x2[-1]-x2[0])/float(n2-1)

masses   = np.array([m1])
npts     = np.array([n1])
spacings = np.array([d1])
E_kin    = e_kin(npts,spacings,masses,order=4)
nstates  = 4

# target density
rho = n_el * np.sum(np.abs(psi[n_state])**2,axis=1)*d2

fref = 1e-4
ftest = 100.
itest = 0

v_ext = V_en
v_hxc = np.zeros(v_ext.shape)
E_pot = pot_sparsify(v_ext+v_hxc)

# solve eigenvalue problem
vals, vecs = linalg.eigsh(E_kin + E_pot,k=nstates,which='SA')
o_ks       = make_psi(vecs,npts,d1,nstates=nstates)
rho_test   = np.abs(o_ks[0])**2+np.abs(o_ks[1])**2

v_hxc      = v_hxc + c_mu * ((rho_test)**c_p - (rho)**c_p)
phi_ks     = np.zeros([nstates,n1])
dphi_ks    = np.zeros([nstates,n1])

plt.figure(figsize=(10,4))
while ftest > fref:
  # solve eigenvalue problem
  E_pot      = pot_sparsify(v_ext+v_hxc)
  vals, vecs = linalg.eigsh(E_kin + E_pot,k=nstates,which='SA')
  o_ks       = make_psi(vecs,npts,d1,nstates=nstates)
  rho_test   = np.abs(o_ks[0])**2 + np.abs(o_ks[1])**2
  ftest      = np.sqrt(np.sum(np.abs(rho_test-rho))*d1)
  v_hxc      = v_hxc + c_mu * ((rho_test)**c_p - (rho)**c_p)
  itest     += 1
  
  if np.mod(itest,50) == 0: 
    print('Currently at %12.8f, target %12.8f ' % (ftest, fref))
    
    # plot
    plt.clf()
    plt.subplot(1,2,1)
    plt.plot(x1,rho)
    plt.plot(x1,rho_test)
    plt.axis([-15,15,0,0.7])
    
    plt.subplot(1,2,2)
    plt.plot(x1,v_ext+v_hxc)
    plt.plot(x1,o_ks[0]*0.3+vals[0])
    plt.plot(x1,o_ks[1]*0.3+vals[1])
    plt.axis([-10,10,-3,0])
    
    plt.pause(.1)

# save data
stuff = {'o_ks':o_ks,'e_ks':vals,'rho':rho,'rho_test':rho_test,'x1':x1,
         'v_hxc':v_hxc,'v_ext':v_ext}
fname = 'data_2e1d_ks_system.h5'
saveToHDF5(fname,stuff)

## plot
#plt.clf()
#plt.subplot(1,3,1)
#plt.plot(x1,rho)
#plt.plot(x1,rho_test)
#plt.axis([-15,15,0,0.7])

#plt.subplot(1,3,2)
#plt.plot(x1,Ve1n+v_hxc,label='KS potential')
#plt.plot(x1,v_ph+v_pk,label='Pauli potential')
#plt.plot(x1,v_hxc+v_ph+v_pk-vals[1],label='OF-DFT')
#plt.plot(data_fac['x1'][::10],(data_fac['v_T'][1,R_ind]+data_fac['v_V'][1,R_ind]+data_fac['v_FS'][1,R_ind]-E[1])[::10],'.',label='EEF')
#plt.axis([-15,15,-6,3])
#plt.legend()

#plt.subplot(1,3,3)
#plt.plot(x1,Ve1n+v_hxc)
#plt.plot(x1,o_ks[0]*0.3+vals[0])
#plt.plot(x1,o_ks[1]*0.3+vals[1])
#plt.axis([-15,15,-6,3])

#plt.savefig('KS_pics/ks_system_%05.2f_%03d.pdf' % (l_ee,R_ind))
#plt.savefig('KS_pics/ks_system_%05.2f_%03d.png' % (l_ee,R_ind))
