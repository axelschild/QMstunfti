===== 
QMstunfti
===== 

Description
-------- 

QMstunfti is a quantum dynamics toolbox written in Python that allows you to set up operators in a (sparse) matrix representations. Functions from the Scipy project can then be used to determine the eigenstates of the operators or to perform a quantum dynamics calculation.

While QMstunfti provides functions to solve quantum problems of arbitrary dimensions, the grid-based representation of the Schroedinger equation will only allow to solve low-dimensional problems numerically. Also, main purpose of QMstunfti is to be multi-purpose, thus using the default tools is probably not the most efficient way to solve the quantum dynamics problem of your choice. However, QMstunfti provides a quick way to solve small quantum problems without much coding effort.

Please check the examples to figure out how QMstunfti works as well as the descrition of the functions. 

How to install CLstunfti
-------- 

1. A few Python packages are needed. Specifically, you should have

- setuptools 
- cython 
- numpy
- scipy
- matplotlib
- h5py
   
installed.

2. Clone or download QMstunfti, e.g. into a folde ``my_python_modules``.
   
3. Compile the cython code bu running

::
      
      python setup.py build_ext --inplace

in the QMstunfti directory.

4. Add the QMstunfti to your python path by writing (adjust the path to the QMstunfti folder) 

::

      export PYTHONPATH=$PYTHONPATH:$HOME/my_python_modules/QMstunfti

in the shell to use it temporary or in your shell configuration file (e.g. .bashrc, .zshrc, .cshrc) to load it when a shell is started.


  
