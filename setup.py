'''
QMstunfti
by Axel Schild

This file is part of QMstunfti.

QMstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

QMstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with QMstunfti.  If not, see <http://www.gnu.org/licenses/>.

Description:
  File for compiling cython code.
  
  python setup.py build_ext --inplace
'''
from setuptools import setup, find_packages
from setuptools.extension import Extension
from Cython.Build import cythonize
import numpy

extensions = [
    Extension("QMstunfti.ptools", ["QMstunfti/ptools/ptools.pyx"],include_dirs=[numpy.get_include()]),
    ]

# compare version numer with __init__.py
setup(
  name       = 'QMstunfti',
  version    ='1.00',
  packages   = find_packages(),
  ext_modules = cythonize(extensions),
)
