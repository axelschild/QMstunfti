import numpy as np
import time
import sys
from numpy.fft import *
try:
  import h5py
except:
  print('Cannot find h5py module. Saving to and loading from HDF5 will not work.')

'''
QMstunfti
by Axel Schild

This file is part of QMstunfti.

QMstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

QMstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with QMstunfti.  If not, see <http://www.gnu.org/licenses/>.


Desription:

Contains some helper files for wavefunction calculations, i.e.
  sort_bo_evib ... for a Born-Oppenheimer calculation of vibrational states on
                   several electronic surfaces, calculate the energetic order 
                   and state indices
  make_psi ....... reshape output of an eigenstate calculation using the 
                   operators in matrix representation to an n-dimensional 
                   array containing the wavefunctions, and renormalize
 
'''

def sort_bo_evib(e_vib):
  ''' 
  PURPOSE: sort list e_vib with shape(e_vib) = (n_ele,n_vib) of n_vib 
  eigenvalues of n_el electronic potentials 
  RETURNS: 
   e_vib_s ... sorted list of eigenvalues
   e_vib_i ... electronic and vibrational state index of the eigenvalues
  '''
  (n_ele,n_vib) = np.shape(e_vib)
  e_vib_i = np.zeros([n_ele,n_vib,2])
  for ii in range(n_ele): e_vib_i[ii,:,0] = ii
  for ii in range(n_vib): e_vib_i[:,ii,1] = ii
  e_vib_i = np.reshape(e_vib_i,[np.prod(np.shape(e_vib)),2])
  e_vib_s = np.reshape(e_vib,np.prod(np.shape(e_vib)))
  e_vib_j = np.argsort(e_vib_s)
  e_vib_s = e_vib_s[e_vib_j]
  e_vib_i = e_vib_i[e_vib_j,:]
  return e_vib_s, e_vib_i

def make_psi(vecs,npts,dv,reverseF=False,nstates=1):
  '''
  Reshape wavefunction from vector to array form
    - assumes vecs is the output of linalg.eigsh
    - will normalize with volume element dv
    - will reshape with shape npts
  '''
  if nstates == 1: 
    vecs = np.reshape(np.squeeze(vecs),(len(vecs),1))
  if reverseF:
    psi = [np.zeros(npts[::-1],dtype=np.complex128) for ii in range(nstates)]
    for ii in range(nstates):
      psi[ii] = np.reshape(vecs[:,ii],npts[::-1],order='F') / np.sqrt(dv)
  else:
    psi = [np.zeros(npts,dtype=np.complex128) for ii in range(nstates)]
    for ii in range(nstates):
      psi[ii] = np.reshape(vecs[:,ii],npts,order='C') / np.sqrt(dv)
  return np.squeeze(psi)

def mask_1d(x,b=0.9,power=0.125):
  '''
    Simple mask function, 1D version. The grid x needs to be symmetric, the 
    mask goes from x[0] to -x[-1]*b and from x[-1]*b to x[-1].
  '''
  xb   = x.max() * b
  den  = 2*(x.max()-xb)
  mask = np.ones(len(x))
  for ii in range(len(x)):
    if   x[ii] >  xb:
      nom = np.pi * ( x[ii]-xb)
      mask[ii] = np.cos(nom/den)**power
    elif x[ii] < -xb:
      nom = np.pi * (-x[ii]-xb)
      mask[ii] = np.cos(nom/den)**power
  return mask

def mask_2d(x,y,bx=0.9,by=0.9,power=0.125):
  mask_x = mask_1d(x,b=bx,power=power)
  mask_y = mask_1d(y,b=by,power=power)
  return np.reshape(mask_x,(len(x),1)) * np.reshape(mask_y,(1,len(y)))
  
