import numpy as np
import scipy.sparse.linalg as linalg
from scipy import sparse as sps
from scipy.interpolate import interp1d
from QMstunfti.matrices import pot_sparsify

'''
QMstunfti
by Axel Schild & Denis Jelovina

This file is part of QMstunfti.

QMstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

QMstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with QMstunfti.  If not, see <http://www.gnu.org/licenses/>.


This file provides some simple propgators for a quantum dynamics simulation.

'''

def prop_expm_multiply(vec,nt_sub=None,t_del=None,H_ti=None,H_td=None,field=None,dipole=None,mask=None):
  '''
  Propagation with scipy.sparse.linalg.expm_multiply with time-dependent 
  potential (but not yet with vector potential).
  '''
  
  # flatten the wavefunction
  
  if (H_td is None) and (field is None):
    # if only the time-independent Hamiltonian is given, use that
    U = -1j * H_ti * t_del
    for i_sub in range(nt_sub):
      vec = linalg.expm_multiply(U,vec)
      if mask is not None: 
        vec *= mask
    
  elif (H_td is not None):
    # if explicit time-dependent Hamiltonian is given, use it
    nt_sub = len(H_td)
    for i_sub in range(nt_sub):
      U   = -1j * (H_ti + H_td[i_sub]) * t_del
      vec = linalg.expm_multiply(U,vec)
      if mask is not None: 
        vec *= mask
    
  elif (field is not None) and (dipole is not None):
    # if laser field is given, assume dipole approximation
    nt_sub = len(field)
    for i_sub in range(nt_sub):
      U   = -1j * (H_ti + pot_sparsify(field[i_sub] * dipole)) * t_del
      vec = linalg.expm_multiply(U,vec)
      if mask is not None: 
        vec *= mask
  
  else:
    print(' Error in calling prop_expm_multiply.                      ')
    print(' How to call it:                                           ')
    print('   Method 1                                                ')
    print('     Provide a time-independent Hamiltonian H_ti=...       ')
    print('   Method 2                                                ')
    print('     Provide a time-independent Hamiltonian H_ti=... and a ')
    print('     time-dependent Hamiltonian H_td=...                   ')
    print('   Method 2                                                ')
    print('     Provide a time-independent Hamiltonian H_ti=... and a ')
    print('     laser field field=... that is only time-dependent and ')
    print('     the dipole operator dipole=...                        ')
  
  return vec

def interplxyxnew(x,y,xnew):
    #interpolate x,y data
    f    = interp1d(x, y, kind='linear')
    ynew = f(xnew)
    return(ynew)

def crank_nicholson(vec,nt_sub,t_del,H_ti,field,dipole,mask=None):
  
  H_ti_2    = H_ti   * 0.5j*t_del
  dipole_2  = dipole * 0.5j*t_del
  ID        = np.identity(H_ti.shape[0])
  H_ti_2s   = sps.csc_matrix(H_ti_2)
  IDs       = sps.csc_matrix(ID)
  dipole_2s = sps.csc_matrix(dipole_2)
  
  nt_sub = len(field)
  
  for i_sub in range(nt_sub):
    Hs  = H_ti_2s + field[i_sub]*dipole_2s
    rhs = vec - Hs.dot(vec)
    LHS = IDs + Hs
    #vec = spsolve(LHS, rhs)
    vec, exitCode = linalg.gmres(LHS,rhs,x0=vec,tol=1e-08) #bicgstab, gmres more accruate but slower
    if(exitCode!=0):
      print("not converged: t=",t)
      return vec
    
    if mask is not None: 
      vec *= mask
    
  return vec
