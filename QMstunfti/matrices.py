import numpy as np
from scipy import sparse as sps
import time

'''
QMstunfti
by Axel Schild (2017 -- )

This file is part of QMstunfti.

QMstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

QMstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with QMstunfti.  If not, see <http://www.gnu.org/licenses/>.


Desription:

Module containing functions to construct matrix representations of operators 
that can be used for eigenstate calculations and time-dependent propagations
in grid based representations of quantum mechanics.

Contains
 gradient_fd .... sparse matrix representation of the gradient operator using 
                  finite differences
 laplacian_fd ... sparse matrix representation of the laplacian operator using 
                  finite differences
                  NOTE: This function can use variable step widths, but this
                        feature is not well tested.
                  NOTE: This function may be used for default or periodic 
                        boundary conditions (more boundary conditions are 
                        implemented but untested).
 e_kin .......... convenience function to construct the matrix representation 
                  of a simple Cartesian kinetic energy operator
 pot_sparsify ... convenience function to construct the matrix representation 
                  of an arbitrary n-dimensional potential
and some examples of potentials for certain selected problems.

'''

def gradient_fd(npts,spacings,speakF=False,order=2,bc=[]):
  ''' 
  Sparse matrix representation of the gradient operator using a finite 
  difference approximation of the derivatives.
  
  NOTE:
  This function has different boundary conditions implemented that can be used 
  by setting the variable bc. The resulting matrix is only anti-hermitian for 
  default and periodic boundary conditions, but not for reflecting boundary
  conditions.
  
  NOTE:
  If reflecting boundary conditions are used, it is assumed that the reflection
  line is half a grid spacing away from the first or last point, respectively.
  With p the points, q the reflected points, and | the reflecting line, for 
  left-reflecting:
                    q   q   q   q | p   p  p   p
  
  If periodic boundary conditions are used, it is assumed that the leftmost
  and rightmost point are on the symmetry line.
  
  RETURNS: 
  M_full ..... len(npts) matrices, where M_full[ii] represents laplacian with 
               respect to coordinate ii, in a sparse martix format
  
  INPUT:
  npts ....... numpy array with number of points in each dimension, e.g. 
               np.array([10,15])
  spacings ... list with len(spacings) = len(npts); each entry is either a 
               number for the grid spacing in the given direction
  order ...... 2, 4, 6, 8, 10; for the respective order of accuracy.
  bc ......... List of strings that selects the boundary conditions for the 
               given spatial direction. If the list is empty, assume that in all
               directions condition 'd' is implied. For each dimension, give one 
               of the following strings:
                'd' .... default, ignore boundaries
                'r' .... reflecting boundary conditions
                'rd' ... left end reflecting, right end default
                'dr' ... left end default, right end reflecting
                'p' .... periodic
  '''
  # make some checks
  npts = np.asarray(npts,dtype='int32')
  for ii in range(len(spacings)):
    spacings[ii] = np.asfarray(spacings[ii])
  try:
    if len(npts) != len(spacings):
      print('!input arrays do not have same length, result may be wrong!')
  except:
    print('!input arrays are probably of wrong type.')
  if not bc:
    bc = ['d' for ii in range(len(npts))]
  if not( isinstance(bc,np.ndarray) or isinstance(bc,list) ):
    print('Variable bc should be a numpy array or a list, even if it has only one entry.')
    print(' Example: bc = ['r'] or bc = np.array(['r']) ')
    print(' The function may work but the results may be wrong...')
  # reverse input order
  spacings = spacings.astype('float64')
  npts = npts[::-1]
  spacings = spacings[::-1]
  # normalization of derivatives
  cfactor = []
  for ii in range(len(npts)):
    if   order == 2: 
      if spacings[ii] == 0: cfactor.append(0)
      else:                 cfactor.append(np.power(spacings[ii],-1)/2.)
    elif order == 4: 
      if spacings[ii] == 0: cfactor.append(0)
      else:                 cfactor.append(np.power(spacings[ii],-1)/12.)
    elif order == 6: 
      if spacings[ii] == 0: cfactor.append(0)
      else:                 cfactor.append(np.power(spacings[ii],-1)/60.)
    elif order == 8: 
      if spacings[ii] == 0: cfactor.append(0)
      else:                 cfactor.append(np.power(spacings[ii],-1)/840.)
    elif order == 10: 
      if spacings[ii] == 0: cfactor.append(0)
      else:                 cfactor.append(np.power(spacings[ii],-1)/2520.)
    else: print ('Order has to be 2, 4, 6, 8 or 10...this will (hopefully) crash...')
  # array containing the full matrices
  M_full = [[] for ii in range(len(npts))]
  # build the derivative matrices for each dimension
  for pdim in range(len(npts)):  
    if speakF == True: start = time.time()
    # size of current submatrix
    full_size = np.prod(npts[0:pdim+1])
    # produce current submatrix M with main diagonal (entries 0)
    M = sps.lil_matrix(0.*sps.eye(full_size))
    # distance between main diagonal and other diagonals
    shift1 = np.prod(npts[0:pdim])
    
    if order == 2:
      # produce other diagonals (with entries -1 (left) and +1 (right))
      for ii in range(shift1,full_size):
        M[ii,ii-shift1] = -1*cfactor[pdim]  
      for ii in range(full_size-shift1):
        M[ii,ii+shift1] =  1*cfactor[pdim]
      # apply boundary conditions
      if bc[pdim] == 'r' or bc[pdim] == 'rd': # reflective boundary conditions for left end
        for ii in range(shift1):
          M[ii,ii] = -cfactor[pdim]
      if bc[pdim] == 'r' or bc[pdim] == 'dr': # reflective boundary conditions for right end
        for ii in range(shift1):
          M[-ii-1,-ii-1] = cfactor[pdim]
      if bc[pdim] == 'p':                     # periodic boundary conditions
        for ii in range(shift1):
          M[-shift1+ii,ii] =  cfactor[pdim]
          M[ii,-shift1+ii] = -cfactor[pdim]
      #if bc[pdim] == 'd':                     # fix boundaries
        #for ii in range(shift1):
          #M[ ii, ii]              = -3*cfactor[pdim]
          #M[ ii, ii+  shift1]     =  4*cfactor[pdim]
          #M[ ii, ii+2*shift1]     = -1*cfactor[pdim]
          #M[-ii-1,-ii-2*shift1-1] =  1*cfactor[pdim]
          #M[-ii-1,-ii-  shift1-1] = -4*cfactor[pdim]
          #M[-ii-1,-ii         -1] =  3*cfactor[pdim]
    
    elif order == 4:
      # produce other diagonals (with entries 1,-8,8,-1)
      for ii in range(2*shift1,full_size):
        M[ii,ii-2*shift1] =  1*cfactor[pdim]
      for ii in range(  shift1,full_size):
        M[ii,ii-  shift1] = -8*cfactor[pdim]
      for ii in range(full_size-shift1):
        M[ii,ii+  shift1] =  8*cfactor[pdim]
      for ii in range(full_size-2*shift1):
        M[ii,ii+2*shift1] = -1*cfactor[pdim]
      # apply boundary conditions
      if bc[pdim] == 'r' or bc[pdim] == 'rd': # reflective boundary conditions for left end
        for ii in range(shift1):
          M[ii,ii         ] = -8 * cfactor[pdim]
          M[ii,ii+  shift1] =  9 * cfactor[pdim]
          M[ii+shift1,ii  ] = -7 * cfactor[pdim]
      if bc[pdim] == 'r' or bc[pdim] == 'dr': # reflective boundary conditions for right end
        for ii in range(shift1):
          M[-ii-1,-ii-1       ] =  8 * cfactor[pdim]
          M[-ii-1,-ii-1-shift1] = -9 * cfactor[pdim]
          M[-ii-1-shift1,-ii-1] =  7 * cfactor[pdim]
      if bc[pdim] == 'p':                     # periodic boundary conditions
        for ii in range(shift1):
          M[-  shift1+ii,ii] =  8 * cfactor[pdim]
          M[ii,-  shift1+ii] = -8 * cfactor[pdim]
        for ii in range(2*shift1):
          M[-2*shift1+ii,ii] =  -cfactor[pdim]
          M[ii,-2*shift1+ii] =   cfactor[pdim]
    
    elif order == 6:
      # produce other diagonals (with entries -1, 9, -45, 45, -9, 1)
      for ii in range(3*shift1,full_size):
        M[ii,ii-3*shift1] =  -1*cfactor[pdim]
      for ii in range(2*shift1,full_size):
        M[ii,ii-2*shift1] =   9*cfactor[pdim]
      for ii in range(  shift1,full_size):
        M[ii,ii-  shift1] = -45*cfactor[pdim]
      for ii in range(full_size-shift1):
        M[ii,ii+  shift1] =  45*cfactor[pdim]
      for ii in range(full_size-2*shift1):
        M[ii,ii+2*shift1] =  -9*cfactor[pdim]
      for ii in range(full_size-3*shift1):
        M[ii,ii+3*shift1] =   1*cfactor[pdim]
      # apply boundary conditions
      if bc[pdim] == 'r' or bc[pdim] == 'rd': # reflective boundary conditions for left end
        for ii in range(shift1):
          M[ii         ,ii         ] = -45 * cfactor[pdim]
          M[ii         ,ii+  shift1] =  54 * cfactor[pdim]
          M[ii         ,ii+2*shift1] = -10 * cfactor[pdim]
          M[ii+  shift1,ii         ] = -36 * cfactor[pdim]
          M[ii+  shift1,ii+  shift1] = - 1 * cfactor[pdim]
          M[ii+2*shift1,ii         ] =   8 * cfactor[pdim]
      if bc[pdim] == 'r' or bc[pdim] == 'dr': # reflective boundary conditions for right end
        for ii in range(shift1):
          M[-ii-1         ,-ii-1         ] =  45 * cfactor[pdim]
          M[-ii-1         ,-ii-1-  shift1] = -54 * cfactor[pdim]
          M[-ii-1         ,-ii-1-2*shift1] =  10 * cfactor[pdim]
          M[-ii-1-  shift1,-ii-1         ] =  36 * cfactor[pdim]
          M[-ii-1-  shift1,-ii-1-  shift1] =   1 * cfactor[pdim]
          M[-ii-1-2*shift1,-ii-1         ] = - 8 * cfactor[pdim]
      if bc[pdim] == 'p':                     # periodic boundary conditions
        for ii in range(  shift1):
          M[-  shift1+ii,ii] = 45 * cfactor[pdim]
          M[ii,-  shift1+ii] =-45 * cfactor[pdim]
        for ii in range(2*shift1):
          M[-2*shift1+ii,ii] = -9*cfactor[pdim]
          M[ii,-2*shift1+ii] =  9*cfactor[pdim]
        for ii in range(3*shift1):
          M[-3*shift1+ii,ii] =  1*cfactor[pdim]
          M[ii,-3*shift1+ii] = -1*cfactor[pdim]
    
    elif order == 8:
      # produce other diagonals (with entries 3, -32, 168, -672, 0, 672, -168, 32, -3)
      for ii in range(4*shift1,full_size):
        M[ii,ii-4*shift1] =    3*cfactor[pdim]
      for ii in range(3*shift1,full_size):
        M[ii,ii-3*shift1] =  -32*cfactor[pdim]
      for ii in range(2*shift1,full_size):
        M[ii,ii-2*shift1] =  168*cfactor[pdim]
      for ii in range(  shift1,full_size):
        M[ii,ii-  shift1] = -672*cfactor[pdim]
      for ii in range(full_size-shift1):
        M[ii,ii+  shift1] =  672*cfactor[pdim]
      for ii in range(full_size-2*shift1):
        M[ii,ii+2*shift1] = -168*cfactor[pdim]
      for ii in range(full_size-3*shift1):
        M[ii,ii+3*shift1] =   32*cfactor[pdim]
      for ii in range(full_size-4*shift1):
        M[ii,ii+4*shift1] =   -3*cfactor[pdim]
      # apply boundary conditions
      if bc[pdim] == 'r' or bc[pdim] == 'rd': # reflective boundary conditions for left end
        for ii in range(shift1):
          M[ii         ,ii         ] = -672 * cfactor[pdim]
          M[ii         ,ii+  shift1] =  840 * cfactor[pdim]
          M[ii         ,ii+2*shift1] = -200 * cfactor[pdim]
          M[ii         ,ii+3*shift1] =   35 * cfactor[pdim]
          M[ii+  shift1,ii         ] = -504 * cfactor[pdim]
          M[ii+  shift1,ii+  shift1] = - 32 * cfactor[pdim]
          M[ii+  shift1,ii+2*shift1] =  675 * cfactor[pdim]
          M[ii+2*shift1,ii         ] =  136 * cfactor[pdim]
          M[ii+2*shift1,ii+  shift1] = -669 * cfactor[pdim]
          M[ii+3*shift1,ii         ] =  -29 * cfactor[pdim]
      if bc[pdim] == 'r' or bc[pdim] == 'dr': # reflective boundary conditions for right end
        for ii in range(shift1):
          M[-ii-1         ,-ii-1         ] =  672 * cfactor[pdim]
          M[-ii-1         ,-ii-1-  shift1] = -840 * cfactor[pdim]
          M[-ii-1         ,-ii-1-2*shift1] =  200 * cfactor[pdim]
          M[-ii-1         ,-ii-1-3*shift1] =  -35 * cfactor[pdim]
          M[-ii-1-  shift1,-ii-1         ] =  504 * cfactor[pdim]
          M[-ii-1-  shift1,-ii-1-  shift1] =   32 * cfactor[pdim]
          M[-ii-1-  shift1,-ii-1-2*shift1] = -675 * cfactor[pdim]
          M[-ii-1-2*shift1,-ii-1         ] = -136 * cfactor[pdim]
          M[-ii-1-2*shift1,-ii-1-  shift1] =  669 * cfactor[pdim]
          M[-ii-1-3*shift1,-ii-1         ] =   29 * cfactor[pdim]
      if bc[pdim] == 'p':                     # periodic boundary conditions
        for ii in range(  shift1):
          M[-  shift1+ii,ii] = 672 * cfactor[pdim]
          M[ii,-  shift1+ii] =-672 * cfactor[pdim]
        for ii in range(2*shift1):
          M[-2*shift1+ii,ii] = -168*cfactor[pdim]
          M[ii,-2*shift1+ii] =  168*cfactor[pdim]
        for ii in range(3*shift1):
          M[-3*shift1+ii,ii] =  32*cfactor[pdim]
          M[ii,-3*shift1+ii] = -32*cfactor[pdim]
        for ii in range(4*shift1):
          M[-4*shift1+ii,ii] =  -3*cfactor[pdim]
          M[ii,-4*shift1+ii] =   3*cfactor[pdim]
    
    elif order == 10:
      # produce other diagonals (with entries -2, 25, -150, 600, -2100, 0, 2100, -600, 150, -25, 2)
      for ii in range(5*shift1,full_size):
        M[ii,ii-5*shift1] =    -2*cfactor[pdim]
      for ii in range(4*shift1,full_size):
        M[ii,ii-4*shift1] =    25*cfactor[pdim]
      for ii in range(3*shift1,full_size):
        M[ii,ii-3*shift1] =  -150*cfactor[pdim]
      for ii in range(2*shift1,full_size):
        M[ii,ii-2*shift1] =   600*cfactor[pdim]
      for ii in range(  shift1,full_size):
        M[ii,ii-  shift1] = -2100*cfactor[pdim]
      for ii in range(full_size-shift1):
        M[ii,ii+  shift1] =  2100*cfactor[pdim]
      for ii in range(full_size-2*shift1):
        M[ii,ii+2*shift1] =  -600*cfactor[pdim]
      for ii in range(full_size-3*shift1):
        M[ii,ii+3*shift1] =   150*cfactor[pdim]
      for ii in range(full_size-4*shift1):
        M[ii,ii+4*shift1] =   -25*cfactor[pdim]
      for ii in range(full_size-5*shift1):
        M[ii,ii+5*shift1] =     2*cfactor[pdim]
      # apply boundary conditions
      if bc[pdim] == 'r' or bc[pdim] == 'rd': # reflective boundary conditions for left end
        for ii in range(shift1):
          M[ii         ,ii         ] = -2100 * cfactor[pdim]
          M[ii         ,ii+  shift1] =  2700 * cfactor[pdim]
          M[ii         ,ii+2*shift1] =  -750 * cfactor[pdim]
          M[ii         ,ii+3*shift1] =   175 * cfactor[pdim]
          M[ii         ,ii+4*shift1] =   -27 * cfactor[pdim]
          M[ii+  shift1,ii         ] = -1500 * cfactor[pdim]
          M[ii+  shift1,ii+  shift1] =  -150 * cfactor[pdim]
          M[ii+  shift1,ii+2*shift1] =  2125 * cfactor[pdim]
          M[ii+  shift1,ii+3*shift1] =  -602 * cfactor[pdim]
          M[ii+2*shift1,ii         ] =   450 * cfactor[pdim]
          M[ii+2*shift1,ii+  shift1] = -2075 * cfactor[pdim]
          M[ii+2*shift1,ii+2*shift1] =    -2 * cfactor[pdim]
          M[ii+3*shift1,ii         ] =  -125 * cfactor[pdim]
          M[ii+3*shift1,ii+  shift1] =   598 * cfactor[pdim]
          M[ii+4*shift1,ii         ] =    23 * cfactor[pdim]
      if bc[pdim] == 'r' or bc[pdim] == 'dr': # reflective boundary conditions for right end
        for ii in range(shift1):
          M[-ii-1         ,-ii-1         ] =  2100 * cfactor[pdim]
          M[-ii-1         ,-ii-1-  shift1] = -2700 * cfactor[pdim]
          M[-ii-1         ,-ii-1-2*shift1] =   750 * cfactor[pdim]
          M[-ii-1         ,-ii-1-3*shift1] =  -175 * cfactor[pdim]
          M[-ii-1         ,-ii-1-4*shift1] =    27 * cfactor[pdim]
          M[-ii-1-  shift1,-ii-1         ] =  1500 * cfactor[pdim]
          M[-ii-1-  shift1,-ii-1-  shift1] =   150 * cfactor[pdim]
          M[-ii-1-  shift1,-ii-1-2*shift1] = -2125 * cfactor[pdim]
          M[-ii-1-  shift1,-ii-1-3*shift1] =   602 * cfactor[pdim]
          M[-ii-1-2*shift1,-ii-1         ] =  -450 * cfactor[pdim]
          M[-ii-1-2*shift1,-ii-1-  shift1] =  2075 * cfactor[pdim]
          M[-ii-1-2*shift1,-ii-1-2*shift1] =     2 * cfactor[pdim]
          M[-ii-1-3*shift1,-ii-1         ] =   125 * cfactor[pdim]
          M[-ii-1-3*shift1,-ii-1-  shift1] =  -598 * cfactor[pdim]
          M[-ii-1-4*shift1,-ii-1         ] =   -23 * cfactor[pdim]
      if bc[pdim] == 'p':                     # periodic boundary conditions
        for ii in range(  shift1):
          M[-  shift1+ii,ii] = 2100 * cfactor[pdim]
          M[ii,-  shift1+ii] =-2100 * cfactor[pdim]
        for ii in range(2*shift1):
          M[-2*shift1+ii,ii] = -600*cfactor[pdim]
          M[ii,-2*shift1+ii] =  600*cfactor[pdim]
        for ii in range(3*shift1):
          M[-3*shift1+ii,ii] =  150*cfactor[pdim]
          M[ii,-3*shift1+ii] = -150*cfactor[pdim]
        for ii in range(4*shift1):
          M[-4*shift1+ii,ii] =  -25*cfactor[pdim]
          M[ii,-4*shift1+ii] =   25*cfactor[pdim]
        for ii in range(5*shift1):
          M[-5*shift1+ii,ii] =    2*cfactor[pdim]
          M[ii,-5*shift1+ii] =   -2*cfactor[pdim]
    
    # repeat the submatrix M as often as necessary
    # FIXME: there seems to be no nice way to use sps.block_diag() to repeat
    # the matrix M as often as needed
    stupid_string = (np.prod(npts[pdim+1:])-1) * 'M,'+'M'
    if pdim == len(npts)-1: M_full[pdim] = sps.coo_matrix(M)
    else: M_full[pdim] = sps.block_diag((eval(stupid_string)))
    if speakF == True: 
      end = time.time()
      print('time needed for dimension '\
            + str(pdim+1) + ': ' + str(end-start) + ' seconds')
  # return full matrices in sparse format
  return M_full[::-1]

def laplacian_fd(npts,spacings,speakF=False,order=2,bc=[],pref=[]):
  '''
  Sparse matrix representation of the laplacian operator (more precise: the 
  second derivative operator in all spatial directions) using a finite 
  difference approximation of the derivatives.
  
  NOTE:
  This function has different boundary conditions implemented that can be used 
  with the variable bc.
  
  RETURNS: 
  M_full ..... len(npts) matrices, where M_full[ii] represents laplacian with 
               respect to coordinate ii, in a sparse martix format
  
  INPUT:
  npts ....... numpy array with number of points in each dimension, e.g. 
               np.array([10,15])
  spacings ... list with len(spacings) = len(npts); each entry is either a 
               number for the grid spacing or a one-dimensional numpy array
               with as many grid spacing as there are points for this directions
               (for variable grid spacing)
  order ...... Either 2 (2nd order) or 4 (4th order approximation).
  bc ......... List of strings that selects the boundary conditions for the 
               given spatial direction. If the list is empty, assume that in all
               directions condition 'd' is implied. For each dimension, give one 
               of the following strings:
                'd' .... default, ignore boundaries
                'r' .... reflecting boundary conditions [never really tested]
                'rd' ... left end reflecting, right end default
                'dr' ... left end default, right end reflecting
                'p' .... periodic
  
  NOTE:
  If reflecting boundary conditions are used, it is assumed that the reflection
  line is half a grid spacing away from the first or last point, respectively.
  With p the points, q the reflected points, and | the reflecting line, for 
  left-reflecting:
                    q   q   q   q | p   p  p   p
  
  If periodic boundary conditions are used, it is assumed that the leftmost
  and rightmost point are on the symmetry line.
  
  
  NOTE:
  The code is written such that it calculates the Laplacian with the first 
  index varying fastest etc. This is called REVERSE order.It will swap the order 
  of the inputs first to make the calculation in  default order with the last 
  index varying fastest.
  
  
  EXPLANAITION:
  Example: 
  Calculate (d**2/dx**2 + d**2/dy**2) f(x,y). We assume that x and y
  are given on a regular grid with grid spacings dx, dy and grid values x(i)
  and y(j). Then f(x(i),y(j))=f(i,j).
  
  From a Taylor expansion of f(x(i)+dx,y(j)) and f(x(i)-dx,y(j)) we obtain up 
  to second order in dx:
  d**2f(i,j)/dx**2  = (f(i-1,j)-2*f(i,j)+f(i+1,j))/(dx**2) + O(dx**2)
  and analogously
  d**2f(i,j)/dy**2  = (f(i,j-1)-2*f(i,j)+f(i,j+1))/(dx**2) + O(dy**2).
  
  For fourth order in dx, we have e.g.:
  d**2f(i,j)/dx**2  = (-f(i-2,j)+16*f(i-1,j)-30*f(i,j)+16*f(i+1,j)-f(i+2,j))
                                                  /(12*dx**2) + O(dx**4)
  
  We now construct a vector g = (f(1,1),f(1,2),...,f(2,1),f(2,2),...) and 
  want to represent 
      d**2f(i,j)/dx**2 + d**2f(i,j)/dy**2
  as 
      g*M_full[0] + g*M_full[1].
  This function constructs the matrices M_full[ii].
  '''
  # make some checks
  npts = np.asarray(npts,dtype='int32')
  for ii in range(len(spacings)):
    spacings[ii] = np.asfarray(spacings[ii])
    if np.squeeze(spacings[ii].shape) and (np.squeeze(spacings[ii].shape) != npts[ii]):
      print('Spacing from dimension %d has the wrong number entries.' % ii)
      print('Should be just a number or have as smany entries as npts[%d].' % ii)
  try:
    if len(npts) != len(spacings):
      print('!input arrays do not have same length, result may be wrong!')
  except:
    print('!input arrays are probably of wrong type.')
  
  if not bc:
    bc = ['d' for ii in range(len(npts))]
  
  # reverse input order (see note above!)
  spacings = spacings.astype('float64')
  npts = npts[::-1]
  spacings = spacings[::-1]
  # normalization of derivatives
  cfactor = []
  for ii in range(len(npts)):
    if   order == 2: 
      cfactor.append(np.power(spacings[ii],-2))
    elif order == 4: 
      cfactor.append(np.power(spacings[ii],-2)/12.)
    elif order == 6: 
      cfactor.append(np.power(spacings[ii],-2)/180.)
    elif order == 8: 
      cfactor.append(np.power(spacings[ii],-2)/5040.)
    else: print ('Order has to be 2, 4, or 6...this will (hopefully) crash...')
  # array containing the full matrices
  M_full = [[] for ii in range(len(npts))]
  # build the derivative matrices for each dimension
  for pdim in range(len(npts)):
    if speakF == True: start = time.time()
    # size of current submatrix
    full_size = np.prod(npts[0:pdim+1])
    
    # SECOND ORDER
    if order == 2:
      # produce current submatrix M with main diagonal (entries -2)
      M = sps.lil_matrix(-2 * cfactor[pdim] * sps.eye(full_size))
      # distance between main diagonal and other diagonals
      shift1 = np.prod(npts[0:pdim])
      print(shift1)
      # produce other diagonals (with entries 1)
      for ii in range(full_size-shift1):
        M[ii,ii+shift1] = cfactor[pdim]
      for ii in range(shift1,full_size):
        M[ii,ii-shift1] = cfactor[pdim]
      # apply boundary conditions
      if bc[pdim] == 'r' or bc[pdim] == 'rd':
        # reflective boundary conditions for left end
        for ii in range(shift1):
          M[ii,ii] = -cfactor[pdim]
      if bc[pdim] == 'r' or bc[pdim] == 'dr':
        # reflective boundary conditions for right end
        for ii in range(shift1):
          M[-ii-1,-ii-1] = -cfactor[pdim]
      if bc[pdim] == 'p':
        # periodic boundary conditions
        for ii in range(shift1):
          M[-shift1+ii,ii] = cfactor[pdim]
          M[ii,-shift1+ii] = cfactor[pdim]
      #if bc[pdim] == 'd':                     # fix boundaries
        #for ii in range(shift1):
          #M[ ii, ii]              =    cfactor[pdim]
          #M[ ii, ii+  shift1]     = -2*cfactor[pdim]
          #M[ ii, ii+2*shift1]     =    cfactor[pdim]
          #M[-ii-1,-ii-2*shift1-1] =    cfactor[pdim]
          #M[-ii-1,-ii-  shift1-1] = -2*cfactor[pdim]
          #M[-ii-1,-ii         -1] =    cfactor[pdim]
          
          #M[ ii, ii]              =  2*cfactor[pdim]
          #M[ ii, ii+  shift1]     = -5*cfactor[pdim]
          #M[ ii, ii+2*shift1]     =  4*cfactor[pdim]
          #M[ ii, ii+3*shift1]     = -1*cfactor[pdim]
          #M[-ii-1,-ii-3*shift1-1] = -1*cfactor[pdim]
          #M[-ii-1,-ii-2*shift1-1] =  4*cfactor[pdim]
          #M[-ii-1,-ii-  shift1-1] = -5*cfactor[pdim]
          #M[-ii-1,-ii         -1] =  2*cfactor[pdim]
    
    # FORTH ORDER
    elif order == 4:
      # produce current submatrix M with main diagonal (entries -30)
      M = sps.lil_matrix(-30 * cfactor[pdim] * sps.eye(full_size))
      # distance between main diagonal and other diagonals
      shift1 = np.prod(npts[0:pdim])
      # produce other diagonals (with entries 16 or -1)
      for ii in range(full_size-shift1):
        M[ii,ii+  shift1] = 16 * cfactor[pdim]
      for ii in range(full_size-2*shift1):
        M[ii,ii+2*shift1] = -1 * cfactor[pdim]
      for ii in range(  shift1,full_size):
        M[ii,ii-  shift1] = 16 * cfactor[pdim]
      for ii in range(2*shift1,full_size):
        M[ii,ii-2*shift1] = -1 * cfactor[pdim]
      # apply boundary conditions
      if bc[pdim] == 'r' or bc[pdim] == 'rd': # reflective boundary conditions for left end
        for ii in range(shift1):
          M[ii,ii         ] = -14 * cfactor[pdim]
          M[ii,ii+  shift1] =  15 * cfactor[pdim]
          M[ii+shift1,ii  ] =  15 * cfactor[pdim]
      if bc[pdim] == 'r' or bc[pdim] == 'dr': # reflective boundary conditions for right end
        for ii in range(shift1):
          M[-ii-1,-ii-1       ] = -14 * cfactor[pdim]
          M[-ii-1,-ii-1-shift1] =  15 * cfactor[pdim]
          M[-ii-1-shift1,-ii-1] =  15 * cfactor[pdim]
      if bc[pdim] == 'p':                     # periodic boundary conditions
        for ii in range(shift1):
          M[-  shift1+ii,ii] =  16 * cfactor[pdim]
          M[ii,-  shift1+ii] =  16 * cfactor[pdim]
        for ii in range(2*shift1):
          M[-2*shift1+ii,ii] =  -cfactor[pdim]
          M[ii,-2*shift1+ii] =  -cfactor[pdim]

    # SITH (sic!) ORDER
    elif order == 6:
      # produce current submatrix M with main diagonal (entries -30)
      M = sps.lil_matrix(-490 * cfactor[pdim] * sps.eye(full_size))
      # distance between main diagonal and other diagonals
      shift1 = np.prod(npts[0:pdim])
      # produce other diagonals (with entries 16 or -1)
      for ii in range(full_size-shift1):
        M[ii,ii+  shift1] = 270 * cfactor[pdim]
      for ii in range(full_size-2*shift1):
        M[ii,ii+2*shift1] = -27 * cfactor[pdim]
      for ii in range(full_size-3*shift1):
        M[ii,ii+3*shift1] =   2 * cfactor[pdim]
      for ii in range(  shift1,full_size):
        M[ii,ii-  shift1] = 270 * cfactor[pdim]
      for ii in range(2*shift1,full_size):
        M[ii,ii-2*shift1] = -27 * cfactor[pdim]
      for ii in range(3*shift1,full_size):
        M[ii,ii-3*shift1] =   2 * cfactor[pdim]
      # apply boundary conditions
      if bc[pdim] == 'r' or bc[pdim] == 'rd': # reflective boundary conditions for left end
        for ii in range(shift1):
          M[ii         ,ii         ] = -220 * cfactor[pdim]
          M[ii         ,ii+  shift1] =  243 * cfactor[pdim]
          M[ii         ,ii+2*shift1] =  -25 * cfactor[pdim]
          M[ii+  shift1,ii         ] =  243 * cfactor[pdim]
          M[ii+  shift1,ii+  shift1] = -488 * cfactor[pdim]
          M[ii+2*shift1,ii         ] =  -25 * cfactor[pdim]
      if bc[pdim] == 'r' or bc[pdim] == 'dr': # reflective boundary conditions for right end
        for ii in range(shift1):
          M[-ii-1         ,-ii-1         ] = -220 * cfactor[pdim]
          M[-ii-1         ,-ii-1-  shift1] =  243 * cfactor[pdim]
          M[-ii-1         ,-ii-1-2*shift1] =  -25 * cfactor[pdim]
          
          M[-ii-1-  shift1,-ii-1         ] =  243 * cfactor[pdim]
          M[-ii-1-  shift1,-ii-1-  shift1] = -488 * cfactor[pdim]
          
          M[-ii-1-2*shift1,-ii-1         ] =  -25 * cfactor[pdim]
      if bc[pdim] == 'p':                     # periodic boundary conditions
        for ii in range(shift1):
          M[-  shift1+ii,ii] =  270 * cfactor[pdim]
          M[ii,-  shift1+ii] =  270 * cfactor[pdim]
        for ii in range(2*shift1):
          M[-2*shift1+ii,ii] =  -27 * cfactor[pdim]
          M[ii,-2*shift1+ii] =  -27 * cfactor[pdim]
        for ii in range(3*shift1):
          M[-3*shift1+ii,ii] =    2 * cfactor[pdim]
          M[ii,-3*shift1+ii] =    2 * cfactor[pdim]

    # repeat the submatrix M as often as necessary
    # FIXME: there seems to be no nice way to use sps.block_diag() to repeat
    # the matrix M as often as needed
    stupid_string = (np.prod(npts[pdim+1:])-1) * 'M,'+'M'
    if pdim == len(npts)-1: M_full[pdim] = sps.coo_matrix(M)
    else: M_full[pdim] = sps.block_diag((eval(stupid_string)))
    
    if speakF == True: 
      end = time.time()
      print('time needed for dimension '\
            + str(pdim+1) + ': ' + str(end-start) + ' seconds')
  # return full matrices in sparse format
  return M_full[::-1]

def e_kin(npts,delta,masses,order=2,bc=[]):
  '''
  Convenience function to compute the sparse matrix representation of a 
  simple Cartesian kinetic energy operator.
  
  Input:
    npts ..... numpy array with the number of points per dimension (even for one 
               dimension, this sould be something like npts = np.array([n])
               although the function tries to change this if you only give a 
               number
    delta .... numpy array with grid spacing per dimension
    masses ... numpy array with masses per dimension
  Output:
    sum_j -1/(2 masses[j]) * laplacian[j]
  '''
  try:
    len(npts)
    L = laplacian_fd(np.array(npts),np.array(delta),order=order,bc=bc)
    return sum(-1/(2*masses)*L)
  except:
    L = laplacian_fd(np.array([npts]),np.array([delta]),order=order,bc=bc)
    return -1/(2*masses)*L

def pot_sparsify(V):
  '''
  Convert an n-dimensional array V to sparse matrix operator representation
  (i.e., construct a sparse matrix that contains the entries of V on its 
  diagonal). By default, row-major order of the input array V is assumed
  (last index runs fastest), but by setting he optional parameter reverseF to
  True this may be changed to column-major order (first index varies fastest).
  '''
  V = np.reshape(V,np.prod(np.shape(V)),order='C')
  return sps.coo_matrix(sps.diags(V,0))
