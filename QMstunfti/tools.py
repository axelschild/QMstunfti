import numpy as np
import time
import sys
from numpy.fft import *
import os
try:
  import h5py
except:
  print('Cannot find h5py module. Saving to and loading from HDF5 will not work.')

'''
QMstunfti
by Axel Schild

This file is part of QMstunfti.

QMstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

QMstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with QMstunfti.  If not, see <http://www.gnu.org/licenses/>.

Desription:

Contains some helper files that are sometimes useful, i.e.
 hdf5_save ... convenicence function to save data to a HDF5 file
 hdf5_load ... convenicence function to load data from a HDF5 file
 ETA ......... simple Estimated Time of Arrival function
 d1_fd ....... first derivative if an array along a given axis using a finite 
               difference approximation
 d2_fd ....... second derivative if an array along a given axis using a finite 
               difference approximation
 d1_ft2d ..... first derivative of a two-axisensional array using FFT
 int_nd ...... integrate array over selected axes using the trapezoidal rule
 
'''

def relative_difference(f1,f2,qname,mode='normal'):
  '''
    Compute maximum relative difference between two quantities.
    If mode == 'abs', we take the absolute value of the difference of the 
    absolute values of the quantities, otherwise it is just the absolute value 
    of the difference of the quantities.
  '''
  if mode == 'abs':
    fmax = np.abs(np.abs(f1)-np.abs(f2))
  else:
    fmax = np.abs(f1-f2)
  imax = np.unravel_index( np.argmax( fmax ), f1.shape)
  rmax = np.abs(fmax[imax]/f1[imax])
  print(' Relative difference between two ways to compute %s: %f' % (qname, rmax))

def make_grid(xmin,xmax,nx,xdim=0,ndim=0):
  '''
  Returns a grid with parameters. If this grid is one dimension of a 
  multidimensional grid, it also returns a grid vector for broadcasting
  (to avoid meshgrid)
  Input:
    xmin ... start value of grid
    xmax ... end value of grid
    nx ..... number of grid points
    xdim ... current dimension of grid (for multidimensional grids), optional
    ndim ... total dimension of grid, optional
  Output:
    x ...... grid
    dx ..... grid spacing
    nx ..... number of grid points 
  Additional output if ndim>0:
    X ...... grid for broadcasting
  '''
  x  = np.linspace(xmin,xmax,nx)
  dx = (x[-1]-x[0])/float(len(x)-1)
  if ndim:
    xsize = [1]*ndim
    xsize[xdim] = nx
    X = np.reshape(x,xsize)
    return x, dx, nx, X
  else:
    return x, dx, nx

class cd:
  '''
  Changing the working directory, inspired by an answer from
  https://stackoverflow.com/questions/431684/how-do-i-cd-in-python
  which is similar to 
  http://code.activestate.com/recipes/576620-changedirectory-context-manager/
  '''
  def __init__(self, directory):
    self.directory = os.path.expanduser(directory)
  def __enter__(self):
    self.cwd = os.getcwd()
    os.chdir(self.directory)
  def __exit__(self, etype, value, traceback):
    os.chdir(self.cwd)


def saveToHDF5(fname,stuff,mode='w'):
  '''
  Save all elements of the dictionary stuff to the HDF5 file fname.
  '''
  f = h5py.File(fname, mode)
  for name, ding in stuff.items():
    ding = np.squeeze(ding)
    if len(np.shape(ding)) == 1:
      dset = f.create_dataset(name,(1,len(ding)),data=ding)
    else:
      dset = f.create_dataset(name,(np.shape(ding)),data=ding)
  f.close()

def loadFromHDF5(fname,stuffList=[]):
  '''
  Load all elements of the dictionary stuff or all elements from the HDF5 file.
  
  Use
   for key,val in data.items(): exec(key + '=val')
  to load all of them in the current workspace.
  '''
  f = h5py.File(fname, 'r')
  returnList = {}
  if stuffList:
    for name in stuffList:
      returnList[name] = np.squeeze(f[name][...])
  else:
    for (name,data) in f.items():
      returnList[name] = np.squeeze(data[...])
  f.close()
  return returnList

def ETA(status,problemSize,timePassed,endF=False,endGuessF=True,skipF=False):
  '''
  Simple Estimated Time of Arrival function
  INPUT:
    status ........ current step
    problemSize ... total number of steps
    timePassed .... time used so far
    endF .......... indicate if this is the last step (see below)
    endGuessF ..... indicate if laststep should be guessed
    skipF ......... indicate if text should be replaced or new line each time
  NOTE:
    By default, a call of ETA will overwrite what is written in the current 
    line. Thus, if ETA is called  successively it will update the estimated 
    time of arrival continuously. If you want to write something between each
    call of ETA, use skipF=True to write the output in a new line without 
    text replacement. By default, ETA will try to guess if the last step is 
    reached and will continue to a new line. If you don't want this, set
    endGuessF=False and set endF=True for the last call of ETA.
  '''
  fraction = status/float(problemSize)
  timeEstimated = timePassed/fraction
  timeToGo = timeEstimated-timePassed
  if skipF:
    sys.stdout.write(\
      '\nDone %(a)5.1f%% using %(b)f minutes lifetime. ETA: %(c)f Minutes'\
      % {'a':fraction*100,'b':timePassed/60.,'c':timeToGo/60.})
  else:
    sys.stdout.write(\
      '\rDone %(a)5.1f%% using %(b)f minutes lifetime. ETA: %(c)f Minutes'\
      % {'a':fraction*100,'b':timePassed/60.,'c':timeToGo/60.})
  sys.stdout.flush()
  if endF or (endGuessF and (1-fraction < 1e-3)):
    print('\n')

def derivative_ft_2d(f,h,n,axis):
  '''
  Very specialized function to compute first derivative of a 2d-array 
  '''
  if axis == 0:
    return ifft2(1j*2*np.pi*np.reshape(fftfreq(n,d=h),(n,1))*fft2(f))
  elif axis == 1:
    return ifft2(1j*2*np.pi*np.reshape(fftfreq(n,d=h),(1,n))*fft2(f))

def drvtv_1(f,h,axis=0,order=2):
  '''
  First derivative of an array computed in a finite difference approximation 
  to various orders. Also the boundaries are computed to the given order.
  '''
  
  # make the axis to be operated on the first axis
  f = np.swapaxes(f,0,axis)
  
  # initialize derivativ
  df = np.zeros(np.shape(f),dtype=f.dtype)
  
  if order not in [2,4,6,8]:
    print(' drvtv_1 WARNING: Only 2nd, 4th, 6th, 8th order derivatives implemented. Falling back to 2nd order.')
    order=2
    
  # first derivative up to second order
  if order == 2:
    for ii in range(len(f)):
      if ii == 0:
        df[ii] = -3*f[ii]+4*f[ii+1]-f[ii+2]
      elif ii == len(f)-1:
        df[ii] =  3*f[ii]-4*f[ii-1]+f[ii-2]
      else:
        df[ii] = -f[ii-1]+f[ii+1]
    df /= float(2*h)
  # first derivative up to fourth order
  elif order == 4:
    for ii in range(len(f)):
      if ii == 0:
        df[ii] = -25*f[ii]+48*f[ii+1]-36*f[ii+2]+16*f[ii+3]-3*f[ii+4]
      elif ii == len(f)-1:
        df[ii] =  25*f[ii]-48*f[ii-1]+36*f[ii-2]-16*f[ii-3]+3*f[ii-4]
      elif ii == 1:
        df[ii] = -3*f[ii-1]-10*f[ii]+18*f[ii+1]-6*f[ii+2]+f[ii+3]
      elif ii == len(f)-2:
        df[ii] =  3*f[ii+1]+10*f[ii]-18*f[ii-1]+6*f[ii-2]-f[ii-3]
      else:
        df[ii] = f[ii-2]-8*f[ii-1]+8*f[ii+1]-f[ii+2]
    df /= float(12*h)
  # first derivative up to sixth order
  elif order == 6:
    for ii in range(len(f)):
      if ii == 0:
        df[ii] = -137*f[ii]+300*f[ii+1]-300*f[ii+2]+200*f[ii+3]-75*f[ii+4]+12*f[ii+5]
      elif ii == len(f)-1:
        df[ii] =  137*f[ii]-300*f[ii-1]+300*f[ii-2]-200*f[ii-3]+75*f[ii-4]-12*f[ii-5]
      elif ii == 1:
        df[ii] = -12*f[ii-1]-65*f[ii]+120*f[ii+1]-60*f[ii+2]+20*f[ii+3]-3*f[ii+4]
      elif ii == len(f)-2:
        df[ii] =  12*f[ii+1]+65*f[ii]-120*f[ii-1]+60*f[ii-2]-20*f[ii-3]+3*f[ii-4]
      elif ii == 2:
        df[ii] =  3*f[ii-2]-30*f[ii-1]-20*f[ii]+60*f[ii+1]-15*f[ii+2]+2*f[ii+3]
      elif ii == len(f)-3:
        df[ii] = -3*f[ii+2]+30*f[ii+1]+20*f[ii]-60*f[ii-1]+15*f[ii-2]-2*f[ii-3]
      else:
        df[ii] = -f[ii-3]+9*f[ii-2]-45*f[ii-1]\
                           +45*f[ii+1]-9*f[ii+2]+f[ii+3]
    df /= float(60*h)
  # first derivative up to eight order
  elif order == 8:
    for ii in range(len(f)):
      if ii == 0:
        df[ii] = 14*(-137*f[ii]+300*f[ii+1]-300*f[ii+2]+200*f[ii+3]-75*f[ii+4]+12*f[ii+5])
      elif ii == len(f)-1:
        df[ii] = 14*( 137*f[ii]-300*f[ii-1]+300*f[ii-2]-200*f[ii-3]+75*f[ii-4]-12*f[ii-5])
      elif ii == 1:
        df[ii] = 14*(-12*f[ii-1]-65*f[ii]+120*f[ii+1]-60*f[ii+2]+20*f[ii+3]-3*f[ii+4])
      elif ii == len(f)-2:
        df[ii] = 14*( 12*f[ii+1]+65*f[ii]-120*f[ii-1]+60*f[ii-2]-20*f[ii-3]+3*f[ii-4])
      elif ii == 2:
        df[ii] = 14*( 3*f[ii-2]-30*f[ii-1]-20*f[ii]+60*f[ii+1]-15*f[ii+2]+2*f[ii+3])
      elif ii == len(f)-3:
        df[ii] = 14*(-3*f[ii+2]+30*f[ii+1]+20*f[ii]-60*f[ii-1]+15*f[ii-2]-2*f[ii-3])
      elif ii == 3:
        df[ii] = 14*(-12*f[ii-1]-65*f[ii]+120*f[ii+1]-60*f[ii+2]+20*f[ii+3]-3*f[ii+4])
      elif ii == len(f)-4:
        df[ii] = 14*( 12*f[ii+1]+65*f[ii]-120*f[ii-1]+60*f[ii-2]-20*f[ii-3]+3*f[ii-4])
      else:
        df[ii] = \
            3*f[ii-4]-32*f[ii-3]+168*f[ii-2]-672*f[ii-1]\
           +672*f[ii+1]-168*f[ii+2]+32*f[ii+3]-3*f[ii+4]
    df /= (840*h)
  
  # return in correct order
  return np.swapaxes(df,0,axis)


def drvtv_2(f,h,axis=0,order=2):
  '''
  Second derivative of an array computed in a finite difference approximation 
  to various orders. Also the boundaries are computed to the given order.
  '''
  
  if order not in [2,4,6,8]:
    print(' drvtv_2 WARNING: Only 2nd, 4th, 6th, 8th order derivatives implemented. Falling back to 2nd order.')
    order=2
  
  f = np.swapaxes(f,0,axis)
  d2f = np.zeros(np.shape(f),dtype=f.dtype)
  # second derivative up to second order
  if order == 2:
    for ii in range(len(f)):
      if ii == 0:
        d2f[ii] = 2*f[ii]-5*f[ii+1]+4*f[ii+2]-f[ii+3]
      elif ii == len(f)-1:
        d2f[ii] = 2*f[ii]-5*f[ii-1]+4*f[ii-2]-f[ii-3]
      else:
        d2f[ii] = f[ii-1]-2*f[ii]+f[ii+1]
    d2f /= float(h**2)
  # second derivative up to fourth order
  elif order == 4:
    for ii in range(len(f)):
      if ii == 0:
        d2f[ii] = 24*f[ii]-60*f[ii+1]+48*f[ii+2]-12*f[ii+3]
      elif ii == len(f)-1:
        d2f[ii] = 24*f[ii]-60*f[ii-1]+48*f[ii-2]-12*f[ii-3]
      elif (ii == 1) or (ii == len(f)-2):
        d2f[ii] = 12*f[ii-1]-24*f[ii]+12*f[ii+1]
      else:
        d2f[ii] = -f[ii-2]+16*f[ii-1]-30*f[ii]+16*f[ii+1]-f[ii+2]
    d2f /= float(12*h**2)
  elif order == 6:
    for ii in range(len(f)):
      if   ii == 0:
        d2f[ii] = 812*f[0]-3132*f[1]+5265*f[2]-5080*f[3]+2970*f[4]-972*f[5]+137*f[6]
      elif ii == 1:
        d2f[ii] = 137*f[ii-1]-147*f[ii+0]-255*f[ii+1]+470*f[ii+2]-285*f[ii+3]+93*f[ii+4]-13*f[ii+5]
      elif ii == 2:
        d2f[ii] = -13*f[ii-2]+228*f[ii-1]-420*f[ii+0]+200*f[ii+1]+15*f[ii+2]-12*f[ii+3]+2*f[ii+4]
      elif ii == len(f)-3:
        d2f[ii] =  2*f[ii-4]-12*f[ii-3]+15*f[ii-2]+200*f[ii-1]-420*f[ii+0]+228*f[ii+1]-13*f[ii+2]
      elif ii == len(f)-2:
        d2f[ii] =  -13*f[ii-5]+93*f[ii-4]-285*f[ii-3]+470*f[ii-2]-255*f[ii-1]-147*f[ii+0]+137*f[ii+1]
      elif ii == len(f)-1:
        d2f[ii] =  137*f[ii-6]-972*f[ii-5]+2970*f[ii-4]-5080*f[ii-3]+5265*f[ii-2]-3132*f[ii-1]+812*f[ii+0]
      else:
        d2f[ii] = (2*f[ii-3]-27*f[ii-2]+270*f[ii-1]-490*f[ii+0]+270*f[ii+1]-27*f[ii+2]+2*f[ii+3])
    d2f /= float(180*h**2)
  elif order == 8:
    for ii in range(len(f)):
      if   ii == 0:
        d2f[ii] = 29531*f[ii+0]-138528*f[ii+1]+312984*f[ii+2]-448672*f[ii+3]+435330*f[ii+4]-284256*f[ii+5]+120008*f[ii+6]-29664*f[ii+7]+3267*f[ii+8]
      elif ii == 1:
        d2f[ii] = 3267*f[ii-1]+128*f[ii+0]-20916*f[ii+1]+38556*f[ii+2]-37030*f[ii+3]+23688*f[ii+4]-9828*f[ii+5]+2396*f[ii+6]-261*f[ii+7]
      elif ii == 2:
        d2f[ii] = -261*f[ii-2]+5616*f[ii-1]-9268*f[ii+0]+1008*f[ii+1]+5670*f[ii+2]-4144*f[ii+3]+1764*f[ii+4]-432*f[ii+5]+47*f[ii+6]
      elif ii == 3:
        d2f[ii] = 47*f[ii-3]-684*f[ii-2]+7308*f[ii-1]-13216*f[ii+0]+6930*f[ii+1]-252*f[ii+2]-196*f[ii+3]+72*f[ii+4]-9*f[ii+5]
      elif ii == len(f)-4:
        d2f[ii] = -9*f[ii-5]+72*f[ii-4]-196*f[ii-3]-252*f[ii-2]+6930*f[ii-1]-13216*f[ii+0]+7308*f[ii+1]-684*f[ii+2]+47*f[ii+3]
      elif ii == len(f)-3:
        d2f[ii] = 47*f[ii-6]-432*f[ii-5]+1764*f[ii-4]-4144*f[ii-3]+5670*f[ii-2]+1008*f[ii-1]-9268*f[ii+0]+5616*f[ii+1]-261*f[ii+2]
      elif ii == len(f)-2:
        d2f[ii] = -261*f[ii-7]+2396*f[ii-6]-9828*f[ii-5]+23688*f[ii-4]-37030*f[ii-3]+38556*f[ii-2]-20916*f[ii-1]+128*f[ii+0]+3267*f[ii+1]
      elif ii == len(f)-1:
        d2f[ii] = 3267*f[ii-8]-29664*f[ii-7]+120008*f[ii-6]-284256*f[ii-5]+435330*f[ii-4]-448672*f[ii-3]+312984*f[ii-2]-138528*f[ii-1]+29531*f[ii+0]
      else:
        d2f[ii] = -9*f[ii-4]+128*f[ii-3]-1008*f[ii-2]+8064*f[ii-1]-14350*f[ii+0]+8064*f[ii+1]-1008*f[ii+2]+128*f[ii+3]-9*f[ii+4]
    d2f /= float(5040*h**2)
    
  return np.swapaxes(d2f,0,axis)

def integrate_over(integrand,delta,indices):
  '''
  Integrate <integrand> over axes given by <indices> with spacings <delta>
  '''
  # if delta is not an array, make it one
  try: nd = len(delta)
  except: nd = 1; delta = [delta]
  delta = np.array(delta)
  indices = np.array(indices)
  # sort indices and delta such that highest index is first in list (because
  # this axis disappears with integration)
  sorter = np.argsort(indices)[::-1]
  indices = indices[sorter]
  delta = delta[sorter]
  # integrate
  for ii, ax in enumerate(indices):
    integrand = np.trapz(integrand,axis=ax,dx=delta[ii])
  return integrand

def ismember(test_items, item_list):
  '''
  ismember function inspired by
  http://stackoverflow.com/questions/15864082/python-equivalent-of-matlabs-ismember-function
  '''
  # get unique items
  item_list_unique = {}
  for ii, item_name in enumerate(item_list):
    if item_name not in item_list_unique:
      item_list_unique[item_name] = 1
  # return array with either 1 or None for each element of test_items
  return np.array([item_list_unique.get(item, None) for item in test_items])

def gridmaker(gmin,gmax,gpts,meshme=False):
  '''
  Convenience function to set up a grid.
  Input:
    gmin ..... start of grid(s), float or tuple of length n
    gmax ..... end   of grid(s), float or tuple of length n
    gpts ..... number of grid points, int or tuple of its of length n
    meshme ... shall a grid for mesh broadcasting be returned? [default: False]
  Output:
    g ...... array grid points or list of n arrays of grid points
    gdel ... grid spacing of list of n grid spacings
  and, if meshme==True:
    G ...... array grid points or list of n arrays of grid points for broadcasting 
  '''
  try:
    len(gpts)
    mdim = True
  except:
    mdim = False
  
  if not mdim:
    g = np.linspace(gmin,gmax,gpts)
    return g
  else:
    g    = []
    gdel = []
    G    = []
    for ii in range(len(gpts)):
      g.append(np.linspace(gmin[ii],gmax[ii],gpts[ii]))
      gdel.append( (gmax[ii]-gmin[ii]) / float(gpts[ii]-1) )
      if meshme:
        gshape = np.ones(len(gpts),dtype=int)
        gshape[ii] = gpts[ii]
        G.append( np.reshape(g[ii],gshape))
    
    if meshme:
      return g, gdel, G
    else:
      return g, gdel
  
