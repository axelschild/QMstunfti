'''
QMstunfti
by Axel Schild

This file is part of QMstunfti.

QMstunfti is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 of 
the License, or any later version.

QMstunfti is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public 
License along with QMstunfti.  If not, see <http://www.gnu.org/licenses/>.

Description:
  Some functions where a loop was a good idea but numpy was needed.

Compile: python setup.py build_ext --inplace

'''

from numpy.random import rand as nrand
import numpy as np
from scipy.optimize import newton
cimport numpy as np
cimport cython

#@cython.boundscheck(False) # turn off bounds-checking for entire function
#@cython.wraparound(False)  # turn off negative index wrapping for entire function
def rk4real_1d(np.ndarray[np.complex128_t,ndim=1] wf,
               np.ndarray[np.float64_t   ,ndim=1] pot,
               np.ndarray[np.float64_t   ,ndim=1] mask,
               np.float64_t                       dx,
               np.float64_t                       dt,
               np.float64_t                       m,
               long                               n_sub ):
    '''
      4th order Runge-Kutta propagation for a 1D wavefunction, fixed potential.
      As RK4 is not norm-conserving, we have to renormalize. For absorbing 
      boundary conditions, you may either set a mask function (which should be 
      1 in the interior region and which should fall of smootly to zero in the 
      absorbing region) or you may use a complex potential, but then you have 
      to use mask = np.ones(len(wf)) as input.
      
      Input:
        wf ...... wavefunction (complex numpy array)
        pot ..... potential (real numpy array)
        mask .... mask for the wavefunction (real numpy array)
        dx ...... grid spacing (float)
        dt ...... time step (float)
        m ....... mass (float)
        n_sub ... number of propagation steps (integer)
    '''
    
    cdef np.ndarray[np.complex128_t,ndim=1] dummy
    cdef np.ndarray[np.complex128_t,ndim=1] dwf
    cdef np.ndarray[np.complex128_t,ndim=1] k1
    cdef np.ndarray[np.complex128_t,ndim=1] k2
    cdef np.ndarray[np.complex128_t,ndim=1] k3
    cdef np.ndarray[np.complex128_t,ndim=1] k4
    cdef long                               ii
    cdef np.float64_t                       im2
    cdef np.float64_t                       norm
    
    # initial norm
    norm = np.sum( np.abs( wf )**2 ) * dx
    # half of inverse mass
    im2 = 1./(2.*m)
    
    for ii in range(n_sub):
      # RK4 propagation part 1
      dummy = wf
      dwf   = drvtv2_1d(dummy,dx)
      k1    = 1j * dt * (im2 * dwf - pot * dummy)
      # RK4 propagation part 2
      dummy = wf + k1/2.
      dwf   = drvtv2_1d(dummy,dx)
      k2    = 1j * dt * (im2 * dwf - pot * dummy)
      # RK4 propagation part 3
      dummy = wf + k2/2.
      dwf   = drvtv2_1d(dummy,dx)
      k3    = 1j * dt * (im2 * dwf - pot * dummy)
      # RK4 propagation part 4
      dummy = wf + k3
      dwf   = drvtv2_1d(dummy,dx)
      k4    = 1j * dt * (im2 * dwf - pot * dummy)
      # RK4 propagation
      wf    = wf + ( k1 + 2*k2 + 2*k3 + k4 ) / 6.
      # normalize
      wf    = wf * np.sqrt( norm / ( np.sum(np.abs(wf)**2)*dx ) )
      # apply mask and get new norm
      wf    = wf * mask
      norm  = np.sum( np.abs( wf )**2 ) * dx
    
    return wf

#@cython.boundscheck(False) # turn off bounds-checking for entire function
#@cython.wraparound(False)  # turn off negative index wrapping for entire function
def rk4real_1d_laser(np.ndarray[np.complex128_t,ndim=1] wf,
                     np.ndarray[np.float64_t   ,ndim=1] pot,
                     np.ndarray[np.float64_t   ,ndim=1] mask,
                     np.ndarray[np.float64_t   ,ndim=1] dip,
                     np.ndarray[np.float64_t   ,ndim=1] field,
                     np.float64_t                       dx,
                     np.float64_t                       dt,
                     np.float64_t                       m,
                     long                               n_sub ):
    '''
    4th order Runge-Kutta propagation for a 1D wavefunction with laser field
    in dipole approximation, length gauge. As RK4 is not norm-conserving, we 
    have to renormalize. For absorbing boundary conditions, you may either set 
    a mask function (which should be 1 in the interior region and which should 
    fall of smootly to zero in the absorbing region) or you may use a complex 
    potential, but then you have to use mask = np.ones(len(wf)) as input.
    
    NOTE:
      H = pot - dip * field
    
    Input:
      wf ...... wavefunction (complex numpy array)
      pot ..... potential (real numpy array)
      mask .... mask for the wavefunction (real numpy array)
      dip ..... dipole (real numpy array)
      field ... laser field (real numpy array, len=n_sub+1, for 0, 1, ..., n_sub+1)
      dx ...... grid spacing (float)
      dt ...... time step (float)
      m ....... mass (float)
      n_sub ... number of propagation steps (integer)
    '''
    
    cdef np.ndarray[np.complex128_t,ndim=1] dummy
    cdef np.ndarray[np.complex128_t,ndim=1] dwf
    cdef np.ndarray[np.complex128_t,ndim=1] k1
    cdef np.ndarray[np.complex128_t,ndim=1] k2
    cdef np.ndarray[np.complex128_t,ndim=1] k3
    cdef np.ndarray[np.complex128_t,ndim=1] k4
    cdef long                               ii
    cdef np.float64_t                       f
    cdef np.float64_t                       im2
    cdef np.float64_t                       norm
    
    # initial norm
    norm = np.sum( np.abs( wf )**2 ) * dx
    # half of inverse mass
    im2 = 1./(2.*m)
    
    for ii in range(n_sub):
      # RK4 propagation part 1
      f     = field[ii]
      dummy = wf
      dwf   = drvtv2_1d(dummy,dx)
      k1    = 1j * dt * (im2 * dwf - (pot - dip*f) * dummy)
      # RK4 propagation part 2
      f     = (field[ii] + field[ii+1])/2.
      dummy = wf + k1/2.
      dwf   = drvtv2_1d(dummy,dx)
      k2    = 1j * dt * (im2 * dwf - (pot - dip*f) * dummy)
      # RK4 propagation part 3
      dummy = wf + k2/2.
      dwf   = drvtv2_1d(dummy,dx)
      k3    = 1j * dt * (im2 * dwf - (pot - dip*f) * dummy)
      # RK4 propagation part 4
      f     = field[ii+1]
      dummy = wf + k3
      dwf   = drvtv2_1d(dummy,dx)
      k4    = 1j * dt * (im2 * dwf - (pot - dip*f) * dummy)
      # RK4 propagation
      wf    = wf + ( k1 + 2*k2 + 2*k3 + k4 ) / 6.
      # normalize
      wf    = wf * np.sqrt( norm / ( np.sum(np.abs(wf)**2)*dx ) )
      # apply mask and get new norm
      wf    = wf * mask
      norm  = np.sum( np.abs( wf )**2 ) * dx
    
    return wf

#@cython.boundscheck(False) # turn off bounds-checking for entire function
#@cython.wraparound(False)  # turn off negative index wrapping for entire function
def rk4real_2d(np.ndarray[np.complex128_t,ndim=2] wf,
               np.ndarray[np.float64_t   ,ndim=2] pot,
               np.ndarray[np.float64_t   ,ndim=1] dx,
               np.ndarray[np.float64_t   ,ndim=1] m,
               np.float64_t                       dt ):
    
    cdef np.ndarray[np.complex128_t,ndim=2] dummy
    cdef np.ndarray[np.complex128_t,ndim=2] dwf
    cdef np.ndarray[np.complex128_t,ndim=2] k1
    cdef np.ndarray[np.complex128_t,ndim=2] k2
    cdef np.ndarray[np.complex128_t,ndim=2] k3
    cdef np.ndarray[np.complex128_t,ndim=2] k4
    
    dummy = wf
    dwf   = drvtv2_2d(dummy,dx[0],0)/m[0] + drvtv2_2d(dummy,dx[1],1)/m[1]
    k1    = 1j * dt * (dwf/2. - pot * dummy)
    
    dummy = wf + k1/2.
    dwf   = drvtv2_2d(dummy,dx[0],0)/m[0] + drvtv2_2d(dummy,dx[1],1)//m[1]
    k2    = 1j * dt * (dwf/2. - pot * dummy)
    
    dummy = wf + k2/2.
    dwf   = drvtv2_2d(dummy,dx[0],0)/m[0] + drvtv2_2d(dummy,dx[1],1)/m[1]
    k3    = 1j * dt * (dwf/2. - pot * dummy)
    
    dummy = wf + k3
    dwf   = drvtv2_2d(dummy,dx[0],0)/m[0] + drvtv2_2d(dummy,dx[1],1)/m[1]
    k4    = 1j * dt * (dwf/2. - pot * dummy)
    
    return wf + ( k1 + 2*k2 + 2*k3 + k4 ) / 6.

#@cython.boundscheck(False) # turn off bounds-checking for entire function
#@cython.wraparound(False)  # turn off negative index wrapping for entire function
def rk4real_2d_lg(np.ndarray[np.complex128_t,ndim=2] wf,
                  np.ndarray[np.float64_t   ,ndim=2] pot,
                  np.ndarray[np.float64_t   ,ndim=2] dip,
                  np.ndarray[np.float64_t   ,ndim=1] dx,
                  np.float64_t                       dt,
                  np.float64_t                       field0,
                  np.float64_t                       field1):
    
    cdef np.float64_t                       field
    cdef np.ndarray[np.complex128_t,ndim=2] dummy
    cdef np.ndarray[np.complex128_t,ndim=2] dwf
    cdef np.ndarray[np.complex128_t,ndim=2] k1
    cdef np.ndarray[np.complex128_t,ndim=2] k2
    cdef np.ndarray[np.complex128_t,ndim=2] k3
    cdef np.ndarray[np.complex128_t,ndim=2] k4
    
    field = field0
    dummy = wf
    dwf   = drvtv2_2d(dummy,dx[0],0) + drvtv2_2d(dummy,dx[1],1)
    k1    = 1j * dt * (dwf/2. - (pot + dip*field) * dummy)
    
    field = (field0 + field1)/2.
    dummy = wf + k1/2.
    dwf   = drvtv2_2d(dummy,dx[0],0) + drvtv2_2d(dummy,dx[1],1)
    k2    = 1j * dt * (dwf/2. - (pot + dip*field) * dummy)
    
    dummy = wf + k2/2.
    dwf   = drvtv2_2d(dummy,dx[0],0) + drvtv2_2d(dummy,dx[1],1)
    k3    = 1j * dt * (dwf/2. - (pot + dip*field) * dummy)
    
    field = field1
    dummy = wf + k3
    dwf   = drvtv2_2d(dummy,dx[0],0) + drvtv2_2d(dummy,dx[1],1)
    k4    = 1j * dt * (dwf/2. - (pot + dip*field) * dummy)
    
    return wf + ( k1 + 2*k2 + 2*k3 + k4 ) / 6.

def drvtv2_1d(np.ndarray[np.complex128_t,ndim=1] f,
                         np.float64_t            dx):
  
  cdef long                               nf
  cdef np.ndarray[np.complex128_t,ndim=1] df
  
  nf = f.size
  df = np.empty(nf,dtype=np.complex128)
  df[0]      = 2*f[0]    - 5*f[1]      + 4*f[2]    - f[3]
  df[1:nf-1] = f[0:nf-2] - 2*f[1:nf-1] + f[2:nf]
  df[nf-1]   = 2*f[nf-1] - 5*f[nf-2]   + 4*f[nf-3] - f[nf-4]
  
  return df / (dx**2)

def drvtv2_2d(np.ndarray[np.complex128_t,ndim=2] f_in,
                         np.float64_t            dx,
                         long                    dim):
  
  cdef long                               nf
  cdef np.ndarray[np.complex128_t,ndim=2] df
  
  # make axis to do derivative the first axis
  f = np.swapaxes(f_in,0,dim)
  
  nf = len(f)
  df = np.empty(f.shape,dtype=np.complex128)
  df[0]      = 2*f[0]    - 5*f[1]      + 4*f[2]    - f[3]
  df[1:nf-1] = f[0:nf-2] - 2*f[1:nf-1] + f[2:nf]
  df[nf-1]   = 2*f[nf-1] - 5*f[nf-2]   + 4*f[nf-3] - f[nf-4]
  
  return np.swapaxes(df,0,dim) / (dx**2)
