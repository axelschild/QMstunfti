import numpy                as np
import matplotlib.pyplot    as plt
from   tools import drvtv_1, drvtv_2
plt.ion()

nx, ny = 300, 100
x = np.linspace(-10,10,nx)
y = np.linspace(-10,10,ny)
X = np.reshape(x,(nx,1))
Y = np.reshape(y,(1,ny))
dx = (x[-1]-x[0]) / float(nx-1)
dy = (y[-1]-y[0]) / float(ny-1)

# reasonably hard test function
f = np.exp(-Y**2+X*Y-X**2/4.) * np.sin(X*Y)

df_dx = (Y * np.cos(X*Y) + (- X/2. +   Y) * np.sin(X*Y) ) * np.exp(-X**2/4.+X*Y-Y**2)
df_dy = (X * np.cos(X*Y) + (  X    - 2*Y) * np.sin(X*Y) ) * np.exp(-X**2/4.+X*Y-Y**2)

df_dx_test = drvtv_1(f,dx,axis=0,order=8)
df_dy_test = drvtv_1(f,dx,axis=1,order=8)

plt.figure(figsize=(10,10))
plt.subplot(3,2,1)
plt.contourf(x,y,df_dx.transpose(),np.linspace(-5,5,100),cmap='RdBu')
plt.subplot(3,2,2)
plt.contourf(x,y,df_dy.transpose(),np.linspace(-5,5,100),cmap='RdBu')
plt.subplot(3,2,3)
plt.contourf(x,y,df_dx_test.transpose(),np.linspace(-5,5,100),cmap='RdBu')
plt.subplot(3,2,4)
plt.contourf(x,y,df_dy_test.transpose(),np.linspace(-5,5,100),cmap='RdBu')
plt.subplot(3,2,5)
plt.contourf(x,y,(df_dx-df_dx_test).transpose(),np.linspace(-5,5,100),cmap='RdBu')
plt.subplot(3,2,6)
plt.contourf(x,y,(df_dy-df_dy_test).transpose(),np.linspace(-5,5,100),cmap='RdBu')

# high
f = np.exp(-x**2) + np.sin(x)
d4f = np.exp(-x**2) * (16*x**4 - 48*x**2 + np.exp(x**2)*np.sin(x) + 12)
d4f_o2 = drvtv_1(drvtv_1(drvtv_1(drvtv_1(f,dx,order=2),dx,order=2),dx,order=2),dx,order=2)
d4f_o4 = drvtv_1(drvtv_1(drvtv_1(drvtv_1(f,dx,order=4),dx,order=4),dx,order=4),dx,order=4)
d4f_o6 = drvtv_1(drvtv_1(drvtv_1(drvtv_1(f,dx,order=6),dx,order=6),dx,order=6),dx,order=6)
d4f_o8 = drvtv_1(drvtv_1(drvtv_1(drvtv_1(f,dx,order=8),dx,order=8),dx,order=8),dx,order=8)

D4f_o2 = drvtv_2(drvtv_2(f,dx,order=2),dx,order=2)
D4f_o4 = drvtv_2(drvtv_2(f,dx,order=4),dx,order=4)
D4f_o6 = drvtv_2(drvtv_2(f,dx,order=6),dx,order=6)
D4f_o8 = drvtv_2(drvtv_2(f,dx,order=8),dx,order=8)


plt.figure()
plt.subplot(2,1,1)
plt.plot(x,d4f,c='gray',lw=3)
plt.plot(x,d4f_o2,'k')
plt.plot(x,d4f_o4,'m')
plt.plot(x,d4f_o6,'c')
plt.plot(x,d4f_o8,'y')

plt.plot(x,D4f_o2,':k')
plt.plot(x,D4f_o4,':m')
plt.plot(x,D4f_o6,':c')
plt.plot(x,D4f_o8,':y')

plt.subplot(2,1,2)
plt.plot(x,d4f-d4f_o2,'k')
plt.plot(x,d4f-d4f_o4,'m')
plt.plot(x,d4f-d4f_o6,'c')
plt.plot(x,d4f-d4f_o8,'y')

plt.plot(x,d4f-D4f_o2,':k')
plt.plot(x,d4f-D4f_o4,':m')
plt.plot(x,d4f-D4f_o6,':c')
plt.plot(x,d4f-D4f_o8,':y')
